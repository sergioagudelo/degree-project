export const environment = {
  production: true,
  database: "mongodb://bookeat:bookeat1@ds035796.mlab.com:35796/bookeat",
  urlBack: "https://bookeat-app.herokuapp.com",
  urlFront: "https://bookeat-app.herokuapp.com",
  urlImgs: "https://bookeat1.s3.amazonaws.com/"
};