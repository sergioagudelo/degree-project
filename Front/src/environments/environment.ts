// Environment files are used to work with local or production stages
// https://github.com/angular/angular-cli/wiki/stories-application-environments
export const environment = {
  production: false,
  database: "mongodb://localhost/restaurantes",
  urlBack: "http://localhost:3000",
  urlFront: "http://localhost:4200",
  urlImgs: "https://bookeat1.s3.amazonaws.com/"
};