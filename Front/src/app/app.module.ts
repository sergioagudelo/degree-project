// Packages
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

// Packages used to work social media login
import { SocialLoginModule, AuthServiceConfig, LinkedInLoginProvider } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider } from "angularx-social-login";

// Pipes and services
import { EmailService } from './services/email.service';
import { DemoNumber } from './pipes/ngFor.pipe';
// Pipe used to filter by name on Home
import { FilterPipeModule } from 'ngx-filter-pipe';

// Components
import { AppComponent } from './app.component';
import { NavbarGeneralComponent } from './components//navbar/navbar-general/navbar-general.component';
import { HomeComponent } from './components//home/home.component';
import { MejorPlatoComponent } from './components//home/mejor-plato/mejor-plato.component';
import { MejorPromoComponent } from './components//home/mejor-promo/mejor-promo.component';
import { MapaComponent } from './components//home/mapa/mapa.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { RestauranteComponent } from './components/restaurante/restaurante.component';
import { HistoriaComponent } from './components/home/historia/historia.component';
import { RestauranteListaComponent } from './components/home/restaurante-lista/restaurante-lista.component';
import { RestauranteService } from './services/restaurante.service';
import { ResenaService } from './services/resena.service';
import { UsuarioService } from './services/usuario.service';
import { ListComponent } from './components/list/list.component';
import { ViewAdminComponent } from './components/view-admin/view-admin.component';
import { LoginComponent } from './components/login/login.component';
import { UserAdminProfileComponent } from './components/login/user-admin-profile/user-admin-profile.component';
import { RestaurantProfileComponent } from './components/login/restaurant-profile/restaurant-profile.component';
import { FooterComponent } from './components/footer/footer.component';

// Used to work social media login 
let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("345116851210-2po4caalbgnrojpkvobqb0ufepf2v1sf.apps.googleusercontent.com")
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("897616747305494")
  }
]);

export function provideConfig() {
  return config;
}

@NgModule({
  // We charge components in declarations
  declarations: [
    AppComponent,    
    NavbarGeneralComponent,
    HomeComponent,
    MejorPlatoComponent,
    MejorPromoComponent,
    MapaComponent,
    ContactoComponent,
    RestauranteComponent,
    HistoriaComponent,
    DemoNumber,
    LoginComponent,
    RestauranteListaComponent,
    ListComponent,
    ViewAdminComponent,
    UserAdminProfileComponent,
    RestaurantProfileComponent,
    FooterComponent
  ],
  // We charge packages in imports
  imports: [
    BrowserModule,
    routing,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5mkOJt6Sc8gmIapSIiLIxxd_S4H1bDTM'
    }),
    FormsModule,
    HttpClientModule,
    SocialLoginModule,
    FilterPipeModule,
    ReactiveFormsModule
  ],
  // We charge Services
  providers: [
    appRoutingProviders,
    //borrar los providers en cada elemento, con este basta
    RestauranteService,
    ResenaService,
    UsuarioService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    EmailService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }