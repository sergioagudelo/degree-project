export class Resena {
    constructor(_id = '', usuario = '', restaurante = '', texto = '', calificacion = '') {
        this._id = _id;
        this.restaurante = restaurante;
        this.usuario = usuario;
        this.texto = texto;
        this.calificacion = calificacion
    }

    _id: String;
    restaurante: String;
    usuario: String;
    texto: String;
    calificacion: String
}