export class Usuario {
    constructor(_id = '', fotoPerfil = '', usuario = '', nombre = '', correo = '', fechaNacimiento = '', celular = '', 
    contrasena = '', bloqueado = false, accessToken = '', provider = 'LOCAL') {
        this._id = _id;
        this.fotoPerfil = fotoPerfil;
        this.usuario = usuario;
        this.nombre = nombre;
        this.correo = correo;
        this.fechaNacimiento = fechaNacimiento;
        this.celular = celular;
        this.contrasena = contrasena;
        this.bloqueado = bloqueado;
        this.accessToken = accessToken;
        this.provider = provider
    }

    _id: String;
    fotoPerfil: string;
    usuario: String;
    nombre: String;
    correo: String;
    fechaNacimiento: String;
    celular: String;
    contrasena: String;
    bloqueado: Boolean;
    accessToken: String;
    provider: string;
}
