import { timeInterval } from 'rxjs/operators'
import { UsuarioService } from '../services/usuario.service'

// This intarface its used to work with the server answer after login or registwer an user
// Interfaces its the other way to manage models, maybe its less stricter than class

// Rev this interface its innecesary, just worked because of time, if we work loginWithRedes on UsuarioService with subscribe we can delete this interface

export interface dataUserJWT{
    dataUser: {
        fotoPerfil: string,
        user: string,
        name: string,
        email: string,
        fechaNacimiento: string,
        celular: string,
        bloqueado: string,
        accessToken: string,
        expiresIn: string
    }
    accessToken: string
}