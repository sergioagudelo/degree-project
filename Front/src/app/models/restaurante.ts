export class Restaurante {
  constructor(_id = '', nombre = '', logo = '', slogan = '', fotoLocal = '', direccion = '', fijo = '',
    celular = '', lat = 0, lng = 0, fotoMenu = '', precioMinimo = 0, horario = null, historia = '',
    categoria = null, servicio = null, metodoPago = null, plato = null, bloqueado = true, nombreContacto = '',
    email = '', telefonoContacto = '',  contrasena = '', accessToken = '') {
    this._id = _id;
    this.nombre = nombre;
    this.logo = logo;
    this.slogan = slogan;
    this.fotoLocal = fotoLocal;
    this.direccion = direccion;
    this.fijo = fijo;
    this.celular = celular;
    this.lat = lat;
    this.lng = lng;
    this.fotoMenu = fotoMenu;
    this.precioMinimo = precioMinimo;
    this.horario = horario;
    this.historia = historia;
    this.categoria = categoria;
    this.servicio = servicio;
    this.metodoPago = metodoPago;
    this.plato = plato;
    this.bloqueado = bloqueado;
    this.nombreContacto = nombreContacto;
    this.email = email;
    this.telefonoContacto = telefonoContacto;
    this.contrasena = contrasena;
    this.accessToken = accessToken;
  }

  _id: string;
  nombre: string;
  logo: string;
  slogan: String;
  fotoLocal: string;
  direccion: String;
  fijo: String;
  celular: String;
  lat: number;
  lng: number;
  fotoMenu: String;
  precioMinimo: Number;
  horario: {};
  historia: String;
  categoria: Array<Object>;
  servicio: Array<Object>;
  metodoPago: Array<Object>;
  plato: Array<Object>;
  bloqueado: Boolean;
  nombreContacto: string;
  email: string;
  telefonoContacto: string;
  contrasena: string;
  accessToken: string;
}