import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import Swal from 'sweetalert2';
import { isNullOrUndefined } from "util";

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

  // Rev not sure why private router works if its supposed that we cannot use private variables on production
  constructor(
    private router: Router
  ){

  }

  canActivate(){
    // Rev fix method, work with accessToken in database, findone by accessToken and username, if exists then he can enter to the route, otherwise not
    // Rev improved way to chekc if user its admin
    if(localStorage.getItem("USER_NAME") == "bookeat"){
        return true;
    }else if (!isNullOrUndefined(localStorage.getItem("USER_NAME"))) {
      // login TRUE
      return true;
    } else {
      Swal.fire('Oops...', 'No te has logueado correctamente', 'error')
      this.router.navigate(['/login']);
      return false;
    }
  }
}