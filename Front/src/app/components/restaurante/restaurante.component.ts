import { Component, OnInit, ɵConsole } from '@angular/core';
// Componentes y modulos para trabajar con el router y parametros
import { Router, ActivatedRoute, Params } from '@angular/router';

import { PLATFORM_SERVER_ID } from '@angular/common/src/platform_id';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';

// Services and models
import { Restaurante } from '../../models/restaurante';
import { Resena } from '../../models/resena';
import { RestauranteService } from '../../services/restaurante.service';
import { ResenaService } from 'src/app/services/resena.service';
import { UsuarioService } from 'src/app/services/usuario.service';

// enviroments for production and local
import { environment } from 'src/environments/environment';

//ruta basica de todas las fotos
const rutaFotos = '/assets/imagenes/';

@Component({
    selector: 'app-restaurante',
    templateUrl: './restaurante.component.html',
    styleUrls: ['./restaurante.component.css']
})

export class RestauranteComponent implements OnInit {
    
    readonly URL_FRONT = environment.urlFront;
    readonly URL_IMGS_LOCAL = environment.urlImgs + 'restaurants/locals/';
    readonly URL_IMGS_LOGO = environment.urlImgs + 'restaurants/logos/';
    readonly URL_IMGS_PLATE = environment.urlImgs + 'restaurants/plates/';

    public fotosSlider: any[];
    public _id;

    // Rev como poner la variable usuario de forma glogal
    public usuario;

    // data for promos
    // object promos
    public promos = {};
    // used to show promos by day
    public promosDay = [];
    // used to save the days on which the restaurant has promos
    public promoDays = [];
    // object plates per category    
    
    // data for plates
    // object plates per category
    public plates = {};
    // used to show plates per category
    public platesPerCategory = [];
    // saves all categories on plates
    public foodCategories = [];

    // resena
    public calificacionRestaurante: number = 0;

    readonly horarioInfo = [];

    constructor(
        // Rev error "ng build --prod --aot" cuando las clases son privadas<
        public restauranteService: RestauranteService,
        public resenaService: ResenaService,
        public usuarioService: UsuarioService,
        public _route: ActivatedRoute,
        public _router: Router
    ) { }

    ngOnInit() {
        //dentro de la funcion de calva usamos una funcion de flecha puesto que necesitamos
        //salir de la funcion para acceder a parametro
        this._route.params.forEach((params: Params) => {
            this._id = params['id'];
        });
        this.getRestaurante();
        this.getResena();

        // Gte the user
        this.usuario = localStorage.getItem('USER_NAME');

        // we Setted variable this.calificacionRestaurante
        this.getCalificacionRestaurante();
    }





    // Redirectin with the router
    // This is a normal method, we can just call it con a (click) directive and the method will
    // redirect us to view restaurant showing the indicated restaurant
    redirection(){
        this._router.navigate(['/restaurante', '5d727cee3ed12a2b10ce3698']);
    }





    resetForm(form?: NgForm) {
        if (form) {
            form.reset();
            this.getResena();
        }
    }




    // used because of error of type Number
    public precioMinimo: number;
    // Restaurantes
    getRestaurante() {
        this.restauranteService.getRestaurante(this._id)
            .subscribe(res => {
                this.restauranteService.selectedRestaurante = res as Restaurante;

                this.precioMinimo = Number(this.restauranteService.selectedRestaurante.precioMinimo);

                // horarioInfo
                for(var dia in res['horario'][0]){
                    this.horarioInfo.push({'dia': dia, 'horario': res['horario'][0][dia]})
                }

                this.createArraySlider(res);
                // call methos to create plates by categoria and promos by day
                this.filteredPromos(res['plato'], res['horario']);
                // create array categories
                this.createCategoriesArray(res['plato']);
                // create array plates per category
                this.filteredPlatesCategory(res['plato']);
            });
    }



    // creates an array for the slider on view info restaurant
    createArraySlider(res){
        this.fotosSlider = [
            [this.URL_IMGS_LOCAL + res['fotoLocal'], res['nombre'], res['slogan']],
        ];
        // restaurants that are already on the random array
        let usedPlates = [];
        // used to know when the max number of restaurants its already reached
        let maxPlatos = false;
        // creates a random array for restaurant slider
        while(usedPlates.length < 5 && maxPlatos == false){
            // generates a random id
            let plato =  Math.floor(Math.random() * res['plato'].length);
            if(!usedPlates.includes(plato)){
                // if plato is promo or not
                let isPromo = res['plato'][plato]['diaPromocion'].length > 0 ? ' - (PROMO)' : '';
                this.fotosSlider.push([
                    this.URL_IMGS_PLATE + res['plato'][plato]['foto'],
                    res['plato'][plato]['nombre'] + " - " + (res['plato'][plato]['precio']) + isPromo,
                    res['plato'][plato]['descripcion']
                ]);

                // true when there are not enough plates to generate de slider with the desired amount
                if(res['plato'].length == usedPlates.length){
                    maxPlatos = true;
                }

                usedPlates.push(plato);
            }
        }
    }






    // returns promos based on day
    getDiaPromo(dia) {
        this.promosDay = this.promos[dia].localPromosDay;
        console.log(this.promos)
        console.log(this.promosDay)
       // this.restauranteService.getDiaPromo(dia, this._id)
       //     .subscribe(res => {
       //         this.restauranteService.plato = res as Restaurante;
       //     });
   }




    // create a new array promos based on platos and diaPromocion
    filteredPromos(platos: any[], horario: any[]){
        // Rev wokr with horario as parameter (its an Object, thats the problem) and not with this horario
        let horario1 = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
        // sets a new array of promos by day
        // extracs every promo by dayPromo
        let localPromosDay = [];
        horario1.forEach(dia => {
            platos.forEach(plato => {
                // sets data for promo
                // Rev once the web page its working with the new kind of restaurants the conditional will be enough with "plato['diaPromocion'] != []"
                // checks if the plate its a promo
                if(plato['diaPromocion'].length > 0){
                    // checks the day on which the plates its a promo
                    if(plato['diaPromocion'].includes(dia)){
                        // this.promos.push(plato);
                        localPromosDay.push(plato);
                    }
                }
            });
            // sets days wit promo
            if(localPromosDay.length > 0){
                this.promos[dia] = { localPromosDay };
                // REV old way to work horario
                // this.promoDays.push(dia['dia'])
                this.promoDays.push(dia)
                localPromosDay = []
            }
        });
    }



localPromosDay
    // create array of categories
    createCategoriesArray(platos: any[]){        
        platos.forEach(plato => {
            plato.categoria.forEach(category => {
                // push every new category
                if(!this.foodCategories.includes(category)){
                    this.foodCategories.push(category);
                }                
            });
        });
    }




    // returns plates based on category
    getCategoriaPlato(categoria) {
        if(categoria == 'view all'){
            // show all the plates
        }else{
            this.platesPerCategory = this.plates[categoria]
        }
        // this.restauranteService.getCategoriaPlato(categoria, this._id)
        //     .subscribe(res => {
        //         this.restauranteService.plato = res as Restaurante;
        //     });
    }
    


    // create a new array plates based on platos and category
    filteredPlatesCategory(platos: any[]){        
        // sets a new array of plates by category
        let tempPlatesPerCategory = []
        this.foodCategories.forEach(category => {
            platos.forEach(plato => {
                if(plato.categoria.includes(category)){
                    tempPlatesPerCategory.push(plato);
                }
            });
            if(tempPlatesPerCategory.length != 0){
                this.plates[category] = tempPlatesPerCategory;
                tempPlatesPerCategory = []
            }
        });
    }






    // minimap titles: resenas
    // gets all the resenhas of a restaurant based on id
    getResena() {
        this.resenaService.getResena(this._id)
            .subscribe(res => {
                this.resenaService.resenas = res as Resena[];
            });
    }





    // Add a new resenha
    addResena(form?: NgForm) {
        form.value.usuario = this.usuario;
        form.value.restaurante = this._id;
        // We cheked if the resena its done corretcly 
        if(form.value.texto){
            if(form.value.calificacion){
                let accessToken = localStorage.getItem("ACCESS_TOKEN");
                this.usuarioService.getUserByAccessToken(accessToken)
                .subscribe(res => {
                    if(res['status'] == 200){
                        this.resenaService.postResena(form.value)
                        .subscribe(res => {
                            //this.getResenas();
                            this.resetForm(form);
                            this.getCalificacionRestaurante();
                        });
                    }else{
                        Swal.fire({
                          title: 'Oops...', 
                          html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
                          '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
                          type: 'error',
                          confirmButtonText: 'OK'
                        }).then((result) => {
                          if (result.value) {
                            this.usuarioService.deleteToken('ACCESS_TOKEN', 'USER_NAME');
                            location.href = this.URL_FRONT + "/login";
                          }
                        });
                      }
                });
            }else{
                Swal.fire('Para realizar una reseña debe calificar el restaurante.', "error", "error");
            }
        }else{
            Swal.fire('Para realizar una reseña debe escribir algo en el cuadro de texto.', "error", "error");
            let inputResena = <HTMLSelectElement> document.getElementById('resena');
            inputResena.focus();
        }
    }





    // Deletes resenha by username and restaurant id
    // Rev its better if we manage reseñar by userdId and not by username
    deleteResenha(idResenha: string){
        Swal.fire({
            title: '¿Desea eliminar la reseña?',
            text: 'Para eliminar la reseña presione el botón "Eliminar."',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.value) {
                let accessToken = localStorage.getItem("ACCESS_TOKEN");
                this.usuarioService.getUserByAccessToken(accessToken)
                .subscribe(res => {
                    // if the user exits
                    if(res['status'] == 200){
                        this.resenaService.deleteResena(idResenha)
                        .subscribe(res => {
                            // if the resenha was correctly deleted
                            if(res['status'] == 200){
                                this.getResena();
                                this.getCalificacionRestaurante();
                                Swal.fire(
                                    res['message'],
                                    'La reseña se ha eliminado satisfactoriamente.',
                                    'success'
                                )
                            }else{
                                Swal.fire(
                                    'Ooops...',
                                    res['message'],
                                    'error'
                                )
                            }
                        })
                      }else{
                        Swal.fire({
                          title: 'Oops...', 
                          html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
                          '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
                          type: 'error',
                          confirmButtonText: 'OK'
                        }).then((result) => {
                          if (result.value) {
                            this.usuarioService.deleteToken('ACCESS_TOKEN', 'USER_NAME');
                            location.href = this.URL_FRONT + "/login";
                          }
                        });
                      }
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              Swal.fire(
                'Proceso cancelado',
                'La reseña no se ha eliminado.',
                'success'
              )
            }
          })
    }






    // returns the calification for the actual restaurant
    getCalificacionRestaurante(){
        this.resenaService.getCalificacionRestaurante(this._id)
            .subscribe(res => {
                this.calificacionRestaurante = res as number;
            });
    }



    // restaurante = {
    //     id: this._id,
    //     nombre: 'El Menor - Burguers',
    //     logo: rutaFotos + 'El Menor - Burguers/fotoLogo.jpeg',
    //     slogan: 'La burguer mas poporra',
    //     fotoRestaurante: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
    //     //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
    //     direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',
    //     fijo: 'ContactoFijo',
    //     celular: '300 6981863',
    //     lat: 7.066814,
    //     lng: -73.082981,

    //     // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
    //     categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
    //     platos: [
    //         {
    //             "nombre": "LA MENORA BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan de finas hierbas, 120 gr de carne, queso doble crema y vegetales acompañado de papa frita.",
    //             "precio": 7500,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "LA POPORRA BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan de finas hierbas, 120 gr de carne, pollo desmenuzado, tocineta, queso doble crema y vegetales acompañado de papa frita.",
    //             "precio": 9000,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "LA POLVORA BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan brioche, 135 gr de carne, doble tocineta, queso doble crema,aros de cebolla en cerveza, mas vegetales acompañado de papa frita.",
    //             "precio": 10000,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "LA MEFIR BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan de finas hierbas, 120 gr de carne, tocineta, queso doble crema, chorizo, pollo desmenuzado mas vegetales acompañado de papa frita.",
    //             "precio": 11000,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "LA MAKIA BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan de finas hierbas, doble carne de 120, pollo desmenuzado, tocineta, queso doble crema y vegetales acompañado de papa frita.",
    //             "precio": 11500,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": ""
    //         },
    //         {
    //             "nombre": "LA MAKIAVELIKA BURGER",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hamburguesa en pan de finas hierbas o brioche, doble carne de 120, pollo desmenuzado, tocineta,chorizo, queso doble crema y vegetales acompañado de papa frita.",
    //             "precio": 14000,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "EL MENOR DOG",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Hot Dog en pan de finas hierbas con salchicha americana, pollo desmenuzado, tocineta, queso gratinado con vegetales acompañado de papas fritas.",
    //             "precio": 7500,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         },
    //         {
    //             "nombre": "LAS MENORAS PAPAS LOCAS",
    //             "categoria": "Comida rápida",
    //             "descripcion": "Papas fritas acompañadas de pollo desmenuzado, Salchicha, tocineta, papa miga y Queso gratinado.",
    //             "precio": 7000,
    //             "foto": "/assets/imagenes/NOMBRE DEL RESTAURANTE/NOMBRE DE LA IMAGEN DE LA FOTO DEL PLATO",
    //             "diaPromocion": "Lunes"
    //         }
    //     ],
    //     fotoMenu: rutaFotos + 'El Menor - Burguers/menuGeneral.jpeg',
    //     precioMinimo: 9000,
    //     horarios: [
    //         {
    //             "dia": "Lunes",
    //             "horario": "18:00 - 00:00"
    //         },
    //         {
    //             "dia": "Martes",
    //             "horario": "18:00 - 00:00"
    //         },
    //         {
    //             "dia": "Miércoles",
    //             "horario": "18:00 - 00:00"
    //         },
    //         {
    //             "dia": "Jueves",
    //             "horario": "18:00 - 00:00"
    //         },
    //         {
    //             "dia": "Viernes",
    //             "horario": "18:00 - 00:00"
    //         },
    //         {
    //             "dia": "Sábado",
    //             "horario": "18:00 - 01:00"
    //         },
    //         {
    //             "dia": "Domingo",
    //             "horario": "18:00 - 01:00"
    //         },
    //         {
    //             "dia": "Festivos",
    //             "horario": "18:00 - 00:00"
    //         }
    //     ],
    //     servicios: ['Baño', 'Servicio2'],
    //     metodosPago: ['Efectivo', 'Metodo2']
    // }
}