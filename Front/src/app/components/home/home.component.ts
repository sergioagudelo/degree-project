import { Component, OnInit , ViewChild} from '@angular/core';
import Swal from 'sweetalert2';

import { RestauranteService } from 'src/app/services/restaurante.service';
import { ResenaService } from 'src/app/services/resena.service';

import { Restaurante } from 'src/app/models/restaurante';
import { RestauranteListaComponent } from './restaurante-lista/restaurante-lista.component';
import { HistoriaComponent } from './historia/historia.component';
import { MejorPlatoComponent } from './mejor-plato/mejor-plato.component';
import { MejorPromoComponent } from './mejor-promo/mejor-promo.component';
import { MapaComponent } from './mapa/mapa.component';
import { componentFactoryName } from '@angular/compiler';
import { scheduleMicroTask } from '@angular/core/src/util';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  readonly URL_IMGS_LOGO = environment.urlImgs + 'restaurants/logos/';

  // Home filter variables
  @ViewChild(RestauranteListaComponent) restaurantesChild: RestauranteListaComponent;
  @ViewChild(HistoriaComponent) historiasChild: HistoriaComponent;
  @ViewChild(MejorPlatoComponent) platosChild: MejorPlatoComponent;
  @ViewChild(MejorPromoComponent) promocionesChild: MejorPromoComponent;
  @ViewChild(MapaComponent) mapaChild: MapaComponent;
  
  categorias: string[];
  filtroPrecios: number[] = [10000, 30000, 60000];
  filtroCalificaion: number[] = [1, 2, 3, 4, 5]
  public restaurantes: Restaurante[] = [];
  public filtroRestaurante: any = {nombre: ''};

  public selectedCategoria: string = "0";
  public selectedPrecio: string = "0";
  public selectedCalificacion: string = "0";
  // Used to filter by open or closed restaurant
  public selectedSchedule:string = "0";
  public currentDate = new Date();
  public arrayDays = ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes" , "Sábado"]


  constructor(
    public restauranteService: RestauranteService,
    public resenaService: ResenaService
  ) { }

  ngOnInit() {
    this.getNombresRestaurantes();
    this.getDifferentCategorias();
  }
  


  
  // minimap titles: filters
  // Returns all the diferent categorias
  getDifferentCategorias() {
    this.restauranteService.getDifferentCategorias()
      .subscribe(res => {
        this.restauranteService.restaurantes = res as Restaurante[];
        this.categorias = res as Array<string>;
      });
  }

  getNombresRestaurantes(){
    this.restauranteService.getRestaurantes().subscribe(res => {
      this.restaurantes = res as Restaurante[];
    });
  }


  // filters restaurants based on schedule
  getRestaurantsBySchedule(restaurantsAboveCalification){
    let openHour;
    let openMinute;
    let closeHour;
    let closeMinute;

    let orderedRestaurants = [];

    let currentDate = {
      day: this.arrayDays[this.currentDate.getDay()],
      hour: this.currentDate.getHours(),
      minute: this.currentDate.getMinutes()
    };

    // its mandatory create orderedRestaurants like this, otherwise orderedRestaurants apuntara a la misma direccion de memoria de restaurants
    // y borrara array en el padre "home"
    restaurantsAboveCalification.forEach(restaurant => {
      if(currentDate){
        // if the reataurant itns close on that day
        if(restaurant.horario[0][currentDate['day']] != 'cerrado'){
          // sets day schedule as time schedule for that day
          // horas de apertura y cierre
          let daySchedule = restaurant.horario[0][currentDate['day']];
          openHour = Number(daySchedule[0] + daySchedule[1]);
          openMinute = Number(daySchedule[3] + daySchedule[4]);            
          closeHour = Number(daySchedule[8] + daySchedule[9]);
          closeMinute = Number(daySchedule[12] + daySchedule[12]);

          if(openHour <= currentDate['hour']){
            // si la hora de apertura es menor que la hora actual, abierto
            if(openHour < currentDate['hour'] || (openHour == currentDate['hour'] && openMinute < currentDate['minute'])){
              if(closeHour > 12 && closeHour > currentDate['hour']){
                // si cierran de noche y si la hora de cierre es mayor que la hora actual, abierto
                if(closeHour > currentDate['hour'] || (closeHour == currentDate['hour'] && closeMinute > currentDate['minute'])){
                  orderedRestaurants.push(restaurant);
                }
              }else if(closeHour < 12){
                // si cierran de madrugada ya estara abierto porque estamos comparando con la hora del dia actual, no la del dia de la madrugada
                // donde la hora de la madrugada siempre sera despues de la hora actual puesto que es al otro dia
                orderedRestaurants.push(restaurant);
              }
            }
          }
        }
      }
    });
    return orderedRestaurants;
  }
  
  // Send the filtered parameters to children of the Home
  // This search its managed like an OR and not like an AND, because there are few restaurants to the degree-project
  getFilteredSearch(){
    if(this.selectedCategoria == "0" && this.selectedPrecio == "0" && this.selectedCalificacion == "0" && this.selectedSchedule == "0"){
      Swal.fire('No ha seleccionado ningun criterio de busqueda.', "La busqueda se realizada sin ningun filtro aplicado.", "warning");
    }

    // restaurantsAboveCalification manage the final resutl of the filtered search
    let restaurantsAboveCalification = null;
    // if user selects calification as filter
    if(this.selectedCalificacion != "0"){
      // first we select the restaurants ids above calification
      this.resenaService.getRestaurantsAboveCalification(parseInt(this.selectedCalificacion)).subscribe(res => {          
        restaurantsAboveCalification = res as Array<{}>;
        this.restauranteService.restaurantes = restaurantsAboveCalification as Restaurante[];
        // then we gets the restaurants based on restaurantsAboveCalification
        // Make sure that restaurantsAboveCalification is sent as a string
        this.restauranteService.getFilteredSearch(this.selectedCategoria, this.selectedPrecio, JSON.stringify(restaurantsAboveCalification)).subscribe(res => {
          restaurantsAboveCalification = res as Restaurante[];
          if(restaurantsAboveCalification.length > 0){
            this.sendFilteredSearchChildren(restaurantsAboveCalification);
          }else{
            Swal.fire('No se han encontrado resultados.', "No tenemos restaurantes que cumplan con tus expectativas :(.");
          }
        }
        );
      }); 
    }else{
      // if user doesn´t select calification as filter
      this.restauranteService.getFilteredSearch(this.selectedCategoria, this.selectedPrecio, this.selectedCalificacion).subscribe(res => {
        restaurantsAboveCalification = res as Restaurante[];

        // if filtered by schedule
        if(this.selectedSchedule != "0"){
          restaurantsAboveCalification = this.getRestaurantsBySchedule(restaurantsAboveCalification);
          console.log(restaurantsAboveCalification)
        }
        
        if(restaurantsAboveCalification.length > 0){
          this.sendFilteredSearchChildren(restaurantsAboveCalification);
        }else{
          Swal.fire('No se han encontrado resultados.', "No tenemos restaurantes que cumplan con tus expectativas :(.");
        }
      });
    }
  }



  // cleans selected values on filter
  cleanFilter(){
    // set local variables to initial values
    this.filtroRestaurante = {nombre: ''};
    this.selectedCategoria = "0";
    this.selectedPrecio = "0";
    this.selectedCalificacion = "0";
    this.selectedSchedule = "0";

    // put select into default value
    let selectCategoria = <HTMLSelectElement> document.getElementById('selectCategoria');
    selectCategoria.selectedIndex = 0;
    let selectPrecio = <HTMLSelectElement> document.getElementById('selectPrecio');
    selectPrecio.selectedIndex = 0;
    let selectCalificacion = <HTMLSelectElement> document.getElementById('selectCalificacion');
    selectCalificacion.selectedIndex = 0;
    let selectedSchedule = <HTMLSelectElement> document.getElementById('selectedSchedule');
    selectedSchedule.selectedIndex = 0;
  }

  // Send data filtered search to every home children
  // Rev this method can be improved using rxjs (observable and BehaviorSubject)
  // Rev imporve method, all the subquerys can be done here in home component
  // Rev improve filter by schedule
  sendFilteredSearchChildren(restaurantsAboveCalification: Restaurante[]){    
    this.restaurantesChild.filteredSearch(restaurantsAboveCalification);
    this.historiasChild.filteredSearch(restaurantsAboveCalification);
    this.platosChild.filteredSearch(restaurantsAboveCalification, this.selectedCategoria, this.selectedPrecio);
    this.promocionesChild.filteredSearch(restaurantsAboveCalification, this.selectedCategoria, this.selectedPrecio);
    this.mapaChild.filteredSearch(restaurantsAboveCalification);
  }

  sendGeolocationChildrenMap(){
    this.mapaChild.geolocation();
    console.log(this.mapaChild.geolocation())
  }

}