import { Component, OnInit } from '@angular/core';
import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from '../../../services/restaurante.service'
import { environment } from 'src/environments/environment';
//ruta basica de todas las fotos
const rutaFotos = '/assets/imagenes/El Menor - Burguers/';

@Component({
  selector: 'app-historia',
  templateUrl: './historia.component.html',
  styleUrls: ['./historia.component.css'],
  providers: [RestauranteService]
})

export class HistoriaComponent implements OnInit {

  readonly URL_IMGS_LOCAL = environment.urlImgs + 'restaurants/locals/';
  
  title: string = 'Los emprendimientos';
  // Only show the three first historys
  threeHistorys = [0, 1, 2];

  constructor(
    public restauranteService: RestauranteService
  ) { }

  ngOnInit() {
    this.getRestauranteHistoria();
    //revisar como tener en cuenta todos los elementos, cuando son 8 solo se muestran 6
  }

  // Rev fix all getRestaurantes on components home, its enough with getting all the restaurants in home componente and sending them to
  // every child component
  getRestauranteHistoria() {
    this.restauranteService.getRestauranteHistoria()
      .subscribe(res => {
        this.randomizeRestaurants(res as Restaurante[])
      });
  }




  // Rev its probable that this method can be a RestauranteService method
  // randomize array restaurants
  randomizeRestaurants(restaurants: Restaurante[], currentDate?){
    let maxLengthRestaurants = 10;
    let orderedRestaurants = [];

    let openHour;
    let openMinute;
    let closeHour;
    let closeMinute;

    // its mandatory create orderedRestaurants like this, otherwise orderedRestaurants apuntara a la misma direccion de memoria de restaurants
    // y borrara array en el padre "home"
    restaurants.forEach(restaurant => {
      // if(currentDate){
      //   // if filtered by current date checks if restaurant.horario matches currentDate        
      //   for(var day in restaurant.horario){
      //     if(currentDate['day'] == restaurant.horario[day]['dia']){
      //       // horas de apertura y cierre
      //       openHour = Number(restaurant.horario[day]['horario'][0] + restaurant.horario[day]['horario'][1]);
      //       openMinute = Number(restaurant.horario[day]['horario'][3] + restaurant.horario[day]['horario'][4]);            
      //       closeHour = Number(restaurant.horario[day]['horario'][8] + restaurant.horario[day]['horario'][9]);
      //       closeMinute = Number(restaurant.horario[day]['horario'][12] + restaurant.horario[day]['horario'][12]);

      //       if(openHour <= currentDate['hour']){
      //         // si la hora de apertura es menor que la hora actual, abierto
      //         if(openHour < currentDate['hour'] || (openHour == currentDate['hour'] && openMinute < currentDate['minute'])){
      //           if(closeHour > 12 && closeHour > currentDate['hour']){
      //             // si cierran de noche y si la hora de cierre es mayor que la hora actual, abierto
      //             if(closeHour > currentDate['hour'] || (closeHour == currentDate['hour'] && closeMinute > currentDate['minute'])){
      //               orderedRestaurants.push(restaurant);
      //             }
      //           }else if(closeHour < 12){
      //             // si cierran de madrugada ya estara abierto porque estamos comparando con la hora del dia actual, no la del dia de la madrugada
      //             // donde la hora de la madrugada siempre sera despues de la hora actual puesto que es al otro dia
      //             orderedRestaurants.push(restaurant);
      //           }
      //         }
      //       }
      //       break;
      //     }
      //   }
      // }else{
        orderedRestaurants.push(restaurant);
      // }
    });

    // restaurants that are already on the random array
    let unorderedRestaurants: Restaurante[] = [];
    // creates a random array for restaurants-list slider until maxLengthRestaurants or orderedRestaurants its empty
    // console.log(orderedRestaurants)
    while(unorderedRestaurants.length < maxLengthRestaurants && orderedRestaurants.length > 0){
      // generates a random id
      let restaurante =  Math.floor(Math.random() * orderedRestaurants.length);
      unorderedRestaurants.push(orderedRestaurants[restaurante]);
      // deletes the aleatory restaurant from orderedRestaurants
      orderedRestaurants.splice(restaurante, 1);
    }

    this.restauranteService.restaurantes = unorderedRestaurants;
  }






  // Minimap titles: filters home
  // Methods used to get filtered restaurants from Home
  filteredSearch(restaurantsAboveCalification: Restaurante[], currentDate?){
    console.log(restaurantsAboveCalification)
    if(restaurantsAboveCalification.length < 3){
      this.threeHistorys = [];
      for(let i = 0; i < restaurantsAboveCalification.length; i++){
        this.threeHistorys.push(i)
      }
    }
    console.log(this.threeHistorys)

    this.randomizeRestaurants(restaurantsAboveCalification, currentDate);
  }




  // Minimap titles: google analitycs
  // send an event to google analitycs
  buttonEventGA(id?: string, restaurante?: string, plato?: string){
    // Close modal
    let modalRestaurant = <HTMLInputElement> document.getElementById('closeModalVerMas');
    modalRestaurant.click();

    // Nosotros
    (<any>window).ga('send', 'event', {
      eventCategory: 'admin',
      eventAction: 'historia',
      eventLabel: id + '-' + restaurante
    });
    // Restaurante
    (<any>window).ga('send', 'event', {
      eventCategory: 'restaurante',
      eventAction: id + '-' + restaurante,
      eventLabel: id + '-' + restaurante + '-historia'
    });
  }
}