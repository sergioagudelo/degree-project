import { Component, OnInit } from '@angular/core';
import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from '../../../services/restaurante.service'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-mejor-promo',
  templateUrl: './mejor-promo.component.html',
  styleUrls: ['./mejor-promo.component.css'],
  providers: [RestauranteService]
})
export class MejorPromoComponent implements OnInit {

  readonly URL_IMGS_PLATE = environment.urlImgs + 'restaurants/plates/';

    public platesPerIndicator = [];
    public cantidadPlatos;
    public cantidadP;

  constructor(
    public restauranteService: RestauranteService
  ) { }

  ngOnInit() {
    this.getRestaurantePlatos();
    this.cantidadPlatos = Array.from(Array(4).keys());
  }


  // Rev fix all getRestaurantes on components home, its enough with getting all the restaurants in home componente and sending them to
  // every child component
  getRestaurantePlatos() {
    // Rev fix method to return only promo platos
    this.restauranteService.getRestaurantePlatos()
      .subscribe(res => {
        this.filteredPromos(res as Restaurante[])
      });
  }




  // create a new array promos based on platos and diaPromocion
  filteredPromos(restaurants: Restaurante[], selectedCategoria?: String, selectedPrecio?: String){
    let ordereredPromos = [];    

    // sets a new array of plates from every restaurant, saves _id and name to work with GA and routerlink
    restaurants.forEach(restaurant => {
      restaurant.plato.forEach(plato => {
        // Rev once the web page its working with the new kind of restaurants the conditional will be enough with "plato['diaPromocion'] != []"
        // plato['diaPromocion'][0] != "" && if working with old restaurants
        if(plato['diaPromocion'].length > 0){
          // if categoria or price match or if categoria or price are not a filter or if categoria or price arent defined initially
          if(plato['categoria'].includes(selectedCategoria) || selectedCategoria == "0" || selectedCategoria == undefined){          
            // Rev fix plato['precio'] change saving precio from string to number
            // that wau we can improve this conditional
            if((Number(plato['precio']) < 1000 && Number(plato['precio']) <= Number(selectedPrecio)/1000) ||
              (Number(plato['precio']) > 1000 && Number(plato['precio']) <= Number(selectedPrecio)) ||
              Number(plato['precio']) <= Number(selectedPrecio)/1000 ||
              selectedPrecio == "0" || selectedPrecio == undefined){
                plato = {
                  plato,
                  restaurante: restaurant._id,
                  restauranteNombre: restaurant.nombre,
                }
                ordereredPromos.push(plato);
            }
          }
        }
      });
    });

    this.randomizePlates(ordereredPromos, selectedCategoria, selectedPrecio)
  }





  // randomize array restaurants.plates
  randomizePlates(ordereredPromos: any[], selectedCategoria: String, selectedPrecio: String){
    let maxLengthPlates = 20;
    this.platesPerIndicator = [];

    // restaurants that are already on the random array
    let unorderedPlates = [];
    // creates a random array for plates-list slider until maxLengthPlates or ordereredPromos its empty
    while(unorderedPlates.length < maxLengthPlates && ordereredPromos.length > 0){
        // generates a random id
        let plate =  Math.floor(Math.random() * ordereredPromos.length);
        // adds the aleatory plate to unorderedPlates
        unorderedPlates.push(ordereredPromos[plate]);
        // deletes the aleatory plate from ordereredPromos
        ordereredPromos.splice(plate, 1);
    }
    
    for(var i = 0; i < Math.trunc(maxLengthPlates/3); i++){
      // tempplatesPerIndicator user to keep three first elements off unorderedPlates
      let tempPlatesPerIndicator = [];
      // generates so many elements three of three as indicators
      for(var j = 0; j < 3; j++){
        if(unorderedPlates[0] == undefined){
          // if there are no more plates then finish adding more plates into this.platesPerIndicator
          i = Math.trunc(maxLengthPlates/3);
          console.log(i)
        }else{
          tempPlatesPerIndicator.push(unorderedPlates[0]);
          unorderedPlates.splice(unorderedPlates[0], 1)
        }
      }
      if(i < Math.trunc(maxLengthPlates/3))
        this.platesPerIndicator.push(tempPlatesPerIndicator);
    }
    console.log(this.platesPerIndicator)
  }





  // Minimap titles: filters home
  // Methods used to get filtered restaurants from Home
  // restaurantsAboveCalification array with filtered restaurants by home filter
  filteredSearch(restaurantsAboveCalification: Restaurante[], selectedCategoria: String, selectedPrecio: String, currentDate?){
    this.filteredPromos(restaurantsAboveCalification as Restaurante[], selectedCategoria, selectedPrecio);
  }





  // Minimap titles: google analitycs
  // send an event to google analitycs
  buttonEventGA(id?: string, restaurante?: string, plato?: string){
    // Nosotros
    (<any>window).ga('send', 'event', {
      eventCategory: 'admin',
      eventAction: 'promo',
      eventLabel: id + '-' + restaurante
    });
    // Restaurante
    (<any>window).ga('send', 'event', {
      eventCategory: 'restaurante',
      eventAction: id + '-' + restaurante,
      eventLabel: id + '-' + plato + '-promo'
    });
  }

  platos = [
    {
      //revisar las categorias, en restaurante tambien toca, como maejar estas categorias
        "idRestaurante": 2,
        "nombre":"LA MENORA BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan de finas hierbas, 120 gr de carne, queso doble crema y vegetales acompañado de papa frita.",
        "precio":7500,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"LA POPORRA BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan de finas hierbas, 120 gr de carne, pollo desmenuzado, tocineta, queso doble crema y vegetales acompañado de papa frita.",
        "precio":9000,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"LA POLVORA BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan brioche, 135 gr de carne, doble tocineta, queso doble crema,aros de cebolla en cerveza, mas vegetales acompañado de papa frita.",
        "precio":10000,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"LA MEFIR BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan de finas hierbas, 120 gr de carne, tocineta, queso doble crema, chorizo, pollo desmenuzado mas vegetales acompañado de papa frita.",
        "precio":11000,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"LA MAKIA BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan de finas hierbas, doble carne de 120, pollo desmenuzado, tocineta, queso doble crema y vegetales acompañado de papa frita.",
        "precio":11500,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":""
    },
    {
        "idRestaurante": 2,
        "nombre":"LA MAKIAVELIKA BURGER",
        "categoria":"Comida rápida",
        "descripcion":"Hamburguesa en pan de finas hierbas o brioche, doble carne de 120, pollo desmenuzado, tocineta,chorizo, queso doble crema y vegetales acompañado de papa frita.",
        "precio":14000,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"EL MENOR DOG",
        "categoria":"Comida rápida",
        "descripcion":"Hot Dog en pan de finas hierbas con salchicha americana, pollo desmenuzado, tocineta, queso gratinado con vegetales acompañado de papas fritas.",
        "precio":7500,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    },
    {
        "idRestaurante": 2,
        "nombre":"LAS MENORAS PAPAS LOCAS",
        "categoria":"Comida rápida",
        "descripcion":"Papas fritas acompañadas de pollo desmenuzado, Salchicha, tocineta, papa miga y Queso gratinado.",
        "precio":7000,
        "foto":"/assets/imagenes/El Menor - Burguers/laPoporra.jpeg",
        "diaPromocion":"Lunes"
    }
  ]

}
