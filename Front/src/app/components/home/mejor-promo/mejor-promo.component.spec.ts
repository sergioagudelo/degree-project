import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MejorPromoComponent } from './mejor-promo.component';

describe('MejorPromoComponent', () => {
  let component: MejorPromoComponent;
  let fixture: ComponentFixture<MejorPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MejorPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MejorPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
