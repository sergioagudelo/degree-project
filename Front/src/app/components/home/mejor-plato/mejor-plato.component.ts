import { Component, OnInit } from '@angular/core';
import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from '../../../services/restaurante.service';
import { environment } from 'src/environments/environment';
// Rev consultas con el services para traerme un solo array restaurantes en todos los componentes

@Component({
    selector: 'app-mejor-plato',
    templateUrl: './mejor-plato.component.html',
    styleUrls: ['./mejor-plato.component.css'],
    providers: [RestauranteService]
})
export class MejorPlatoComponent implements OnInit {

  readonly URL_IMGS_PLATE = environment.urlImgs + 'restaurants/plates/';

  public platesPerIndicator = [];

  constructor(
    public restauranteService: RestauranteService
  ) { }

  ngOnInit() {
    this.getRestaurantePlatos();
  }

  // Rev fix all getRestaurantes on components home, its enough with getting all the restaurants in home componente and sending them to
  // every child component
  getRestaurantePlatos() {
    this.restauranteService.getRestaurantePlatos()
      .subscribe(res => {
        this.randomizePlates(res as Restaurante[]);
      });
  }





  // randomize array restaurants.plates
  randomizePlates(restaurants: Restaurante[], selectedCategoria?: String, selectedPrecio?: String){
    let maxLengthPlates = 60;
    let ordereredPlates = [];
    this.platesPerIndicator = [];
    
    // sets a new array of plates from every restaurant, saves _id and name to work with GA and routerlink
    restaurants.forEach(restaurant => {
      restaurant.plato.forEach(plato => {
        // if categoria or price match or if categoria or price are not a filter or if categoria or price arent defined initially
        if(plato['categoria'].includes(selectedCategoria) || selectedCategoria == "0" || selectedCategoria == undefined){          
          // Rev fix plato['precio'] change saving precio from string to number
          // that wau we can improve this conditional
          // if((Number(plato['precio']) < 1000 && Number(plato['precio']) <= Number(selectedPrecio)/1000) ||
          //   (Number(plato['precio']) > 1000 && Number(plato['precio']) <= Number(selectedPrecio)) ||
          //   Number(plato['precio']) <= Number(selectedPrecio)/1000 ||
          //   selectedPrecio == "0" || selectedPrecio == undefined){
          if((Number(plato['precio']) < 1000 && Number(plato['precio']) <= Number(selectedPrecio)/1000) ||
            (Number(plato['precio']) > 1000 && Number(plato['precio']) <= Number(selectedPrecio)) ||
            Number(plato['precio']) <= Number(selectedPrecio)/1000 ||
            selectedPrecio == "0" || selectedPrecio == undefined){
            plato = {
              plato,
              restaurante: restaurant._id,
              restauranteNombre: restaurant.nombre,
            }
            ordereredPlates.push(plato);
          }
        }
      });
    });

    // restaurants that are already on the random array
    let unorderedPlates = [];
    // creates a random array for plates-list slider until maxLengthPlates or ordereredPlates its empty
    while(unorderedPlates.length < maxLengthPlates && ordereredPlates.length > 0){
        // generates a random id
        let plate =  Math.floor(Math.random() * ordereredPlates.length);
        // adds the aleatory plate to unorderedPlates
        unorderedPlates.push(ordereredPlates[plate]);
        // deletes the aleatory plate from ordereredPlates
        ordereredPlates.splice(plate, 1);
    }

    for(var i = 0; i < Math.trunc(maxLengthPlates/3); i++){
      // tempplatesPerIndicator user to keep three first elements off unorderedPlates
      let tempPlatesPerIndicator = [];
      // generates so many elements three of three as indicators
      for(var j = 0; j < 3; j++){
        if(unorderedPlates[0] == undefined){
          // if there are no more plates then finish adding more plates into this.platesPerIndicator
          i = Math.trunc(maxLengthPlates/3);
        }
        tempPlatesPerIndicator.push(unorderedPlates[0]);
        unorderedPlates.splice(unorderedPlates[0], 1)
      }
      this.platesPerIndicator.push(tempPlatesPerIndicator);
    }
  }






  // Minimap titles: filters home
  // Methods used to get filtered restaurants from Home
  // restaurantsAboveCalification array with filtered restaurants by home filter
  filteredSearch(restaurantsAboveCalification: Restaurante[], selectedCategoria: String, selectedPrecio: String, currentDate?){
    this.randomizePlates(restaurantsAboveCalification as Restaurante[], selectedCategoria, selectedPrecio);
  }





  // Minimap titles: google analitycs
  // send an event to google analitycs
  buttonEventGA(id?: string, restaurante?: string, plato?: string){
    // Nosotros
    (<any>window).ga('send', 'event', {
      eventCategory: 'admin',
      eventAction: 'plato',
      eventLabel: id + '-' + restaurante
    });
    // Restaurante
    (<any>window).ga('send', 'event', {
      eventCategory: 'restaurante',
      eventAction: id + '-' + restaurante,
      eventLabel: id + '-' + plato + '-plato'
    });
  }
}