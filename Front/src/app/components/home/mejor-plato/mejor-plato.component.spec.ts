import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MejorPlatoComponent } from './mejor-plato.component';

describe('MejorPlatoComponent', () => {
  let component: MejorPlatoComponent;
  let fixture: ComponentFixture<MejorPlatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MejorPlatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MejorPlatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
