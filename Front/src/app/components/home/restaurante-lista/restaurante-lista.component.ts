import { Component, OnInit, ɵConsole } from '@angular/core';
import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from '../../../services/restaurante.service'
import { FORMERR } from 'dns';
import { environment } from 'src/environments/environment';

const rutaFotos = '/assets/imagenes/';

@Component({
  selector: 'app-restaurante-lista',
  templateUrl: './restaurante-lista.component.html',
  styleUrls: ['./restaurante-lista.component.css'],
  providers: [RestauranteService]
})
export class RestauranteListaComponent implements OnInit {

  readonly URL_IMGS_LOCAL = environment.urlImgs + 'restaurants/locals/';
  
  constructor(
        // revisar error "ng build --prod --aot" cuando las clases son privadas  
        public restauranteService: RestauranteService
      ) { }

  ngOnInit() {
    this.getRestaurantes();
  }

  // Rev fix all getRestaurantes on components home, its enough with getting all the restaurants in home componente and sending them to
  // every child component
  getRestaurantes() {
    this.restauranteService.getRestaurantesWithCalifications()
      .subscribe(res => {
        this.randomizeRestaurants(res as Restaurante[])
      });
  }




  
  // Rev its probable that this method can be a RestauranteService method
  // randomize array restaurants
  randomizeRestaurants(restaurants: Restaurante[], currentDate?){
    let maxLengthRestaurants = 10;
    let orderedRestaurants = [];

    // its mandatory create orderedRestaurants like this, otherwise orderedRestaurants apuntara a la misma direccion de memoria de restaurants
    // y borrara array en el padre "home"
    restaurants.forEach(restaurant => {
      // if(currentDate){
      //   // if the reataurant itns close on that day
      //   if(restaurant.horario[0][currentDate['day']] != 'cerrado'){
      //     // sets day schedule as time schedule for that day
      //     // horas de apertura y cierre
      //     let daySchedule = restaurant.horario[0][currentDate['day']];
      //     openHour = Number(daySchedule[0] + daySchedule[1]);
      //     openMinute = Number(daySchedule[3] + daySchedule[4]);            
      //     closeHour = Number(daySchedule[8] + daySchedule[9]);
      //     closeMinute = Number(daySchedule[12] + daySchedule[12]);

      //     console.log(openHour,
      //       openMinute,
      //       closeHour,
      //       closeMinute)

      //     if(openHour <= currentDate['hour']){
      //       // si la hora de apertura es menor que la hora actual, abierto
      //       if(openHour < currentDate['hour'] || (openHour == currentDate['hour'] && openMinute < currentDate['minute'])){
      //         if(closeHour > 12 && closeHour > currentDate['hour']){
      //           // si cierran de noche y si la hora de cierre es mayor que la hora actual, abierto
      //           if(closeHour > currentDate['hour'] || (closeHour == currentDate['hour'] && closeMinute > currentDate['minute'])){
      //             orderedRestaurants.push(restaurant);
      //           }
      //         }else if(closeHour < 12){
      //           // si cierran de madrugada ya estara abierto porque estamos comparando con la hora del dia actual, no la del dia de la madrugada
      //           // donde la hora de la madrugada siempre sera despues de la hora actual puesto que es al otro dia
      //           orderedRestaurants.push(restaurant);
      //         }
      //       }
      //     }
      //   }
        
      // }else{
        orderedRestaurants.push(restaurant);
      // }
    });

    // restaurants that are already on the random array
    let unorderedRestaurants: Restaurante[] = [];
    // creates a random array for restaurants-list slider until maxLengthRestaurants or orderedRestaurants its empty
    while(unorderedRestaurants.length < maxLengthRestaurants && orderedRestaurants.length > 0){
      // generates a random id
      let restaurante =  Math.floor(Math.random() * orderedRestaurants.length);
      unorderedRestaurants.push(orderedRestaurants[restaurante]);      
      // deletes the aleatory restaurant from orderedRestaurants
      orderedRestaurants.splice(restaurante, 1);
    }

    this.restauranteService.restaurantes = unorderedRestaurants;    
  }
  




  // Minimap titles: filters home
  // Methods used to get filtered restaurants from Home
  filteredSearch(restaurantsAboveCalification: Restaurante[], currentDate?){
    this.randomizeRestaurants(restaurantsAboveCalification, currentDate);
  }





  // Minimap titles: google analitycs
  // send an event to google analitycs
  buttonEventGA(id?: string, restaurante?: string, plato?: string){
    // Nosotros
    (<any>window).ga('send', 'event', {
      eventCategory: 'admin',
      eventAction: 'restaurante',
      eventLabel: id + '-' + restaurante
    });
    // Restaurante
    (<any>window).ga('send', 'event', {
      eventCategory: 'restaurante',
      eventAction: id + '-' + restaurante,
      eventLabel: id + '-' + restaurante + '-restaurante'
    });
  }
  
//   restaurantes = [
//     {
//       id: 1,
//       nombre: 'El Menor - Burguers1',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     },
//     {
//       id: 2,
//       nombre: 'El Menor - Burguers2',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     },
//     {
//       id: 3,
//       nombre: 'El Menor - Burguers3',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     },
//     {
//       id: 4,
//       nombre: 'El Menor - Burguers4',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     },
//     {
//       id: 5,
//       nombre: 'El Menor - Burguers5',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     },
//     {
//       id: 6,
//       nombre: 'El Menor - Burguers6',
//       fotoLocal: rutaFotos + 'El Menor - Burguers/fotoRestaurante.jpeg',
//       //Ciudad, barrio, carrera, calle, casa, conjunto, torre, apto
//       direccion: 'Floridablanca, Bucarica, carrera, calle, casa, Bloque #20-3, Local 101',

//       // SELECT DISTINCT CATEGORIAS PROM PLATFORM_SERVER_ID.CATEGORIA
//       categorias: ['CATEGORIA1', 'CATEGORIA2', 'CATEGORIA3', 'CATEGORIA4', 'CATEGORIA5', 'CATEGORIA6'],
//       precioMinimo: 9000,
//       horarios: [
//           {
//               "dia": "Lunes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Martes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Miércoles",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Jueves",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Viernes",
//               "horario": "18:00 - 00:00"
//           },
//           {
//               "dia": "Sábado",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Domingo",
//               "horario": "18:00 - 01:00"
//           },
//           {
//               "dia": "Festivos",
//               "horario": "18:00 - 00:00"
//           }
//       ],
//       metodosPago: ['Efectivo', 'Metodo2']
//     }
//   ]

}
