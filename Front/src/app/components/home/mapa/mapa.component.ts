import { Component, OnInit, ViewChild } from '@angular/core';
import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from '../../../services/restaurante.service'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css'],
  providers: [RestauranteService]
})

export class MapaComponent implements OnInit {
  
  readonly URL_IMGS_LOCAL = environment.urlImgs + 'restaurants/locals/';

  title: string = 'Encuentra tu restarante favorito';
  subtitle: string = 'Puede estar más cerca de lo que te imaginas'
  zoom: number = 13;
  // Map centered on Bucaramanga unless user allow us to work with his position
  latMap: number = 7.120225;
  lngMap: number = -73.116154;
  markers = [];


  constructor(
    // Rev error "ng build --prod --aot" cuando las clases son privadas<
    public restauranteService: RestauranteService
  ) { }

  ngOnInit() {
    // Sets properties to maps and create map
    const mapProperties = {
      center: new google.maps.LatLng(this.latMap, this.lngMap),
      zoom: this.zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

    // Get restaurants
    this.getRestauranteMapa();
  }


  
  // Minimap titles: google maps
  // initialize the map within the OnInit lifecycle hook
  // We can work directrly with js google maps api: ej. https://stackoverflow.com/questions/39399576/angularjs-google-maps-info-window
  @ViewChild('map') mapElement: any;
  map: google.maps.Map;
  infoWindow = new google.maps.InfoWindow;

  geolocation(){
    // Geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        var pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        this.infoWindow.setPosition(pos);
        this.infoWindow.setContent('Estás aqui.');
        this.infoWindow.open(this.map);
        this.map.setCenter(pos);
        this.map.setZoom(17);


        // used for agm-maps
        this.zoom = 15;
        this.latMap = pos.lat;
        this.lngMap = pos.lng;


      }, () => {
        this.handleLocationError(true, this.infoWindow, this.map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError(false, this.infoWindow, this.map.getCenter());
    }
  }

  // Create every marker on the map
  createMarkers(restaurantes: Restaurante[]){
    for(let restaurante of restaurantes){
      var marker = new google.maps.Marker({
        // + Used to parse to Number. showldn´t be neccesary because restaurante.lat.lng are number but cosole sends error
        position: {
          lat: +restaurante.lat,
          lng: +restaurante.lng
        },
        map: this.map,
        // title: restaurante.nombre
        icon: 'favicon.ico'
      });

      // sets a label
      // var infowindow = new google.maps.InfoWindow({
      //   content: "hol"
      // });

      // marker.addListener('click', function() {
      //   infowindow.open(this.map, marker);
      // });
      this.markers.push(marker);
    }
    this.setMapOnAll(this.map);
  }

  // Sets the map on all markers in the array, neccesari to work with filtered search
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }
  // Send error if geolocation with gps dosen´t work
  handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(this.map);
  }






  // Restaurants CRUD
  // Gets all the restaurans
  getRestauranteMapa() {
    this.restauranteService.getRestauranteMapa()
      .subscribe(res => {
        this.restauranteService.restaurantes = res as Restaurante[];
        this.createMarkers(this.restauranteService.restaurantes);
      });
  }



  // Minimap titles: filters home
  // Methods used to get filtered restaurants from Home
  filteredSearch(restaurantsAboveCalification: Restaurante[], currentDate?){
    this.restauranteService.restaurantes = restaurantsAboveCalification;
    // Clears map to draw new filtered markers
    this.setMapOnAll(null)
    this.markers = [];
    // Set new map
    this.createMarkers(restaurantsAboveCalification);
  }





  // Minimap titles: google analitycs
  // send an event to google analitycs
  buttonEventGA(id?: string, restaurante?: string, plato?: string){
    // Nosotros
    (<any>window).ga('send', 'event', {
      eventCategory: 'admin',
      eventAction: 'mapa',
      eventLabel: id + '-' + restaurante
    });
    // Restaurante
    (<any>window).ga('send', 'event', {
      eventCategory: 'restaurante',
      eventAction: id + '-' + restaurante,
      eventLabel: id + '-' + restaurante + '-mapa'
    });
  }

}