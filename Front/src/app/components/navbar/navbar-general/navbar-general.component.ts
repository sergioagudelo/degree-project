import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
// import { Usuario } from 'src/app/models/usuario';
// Packages used to work social media login
import { AuthService } from "angularx-social-login";
// From AuthService with social media we receive a SocialUser object
import { SocialUser } from "angularx-social-login";
import { RestauranteService } from 'src/app/services/restaurante.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
// import { Restaurante } from 'src/app/models/restaurante';

@Component({
  selector: 'app-navbar-general',
  templateUrl: './navbar-general.component.html',
  styleUrls: ['./navbar-general.component.css']
})
export class NavbarGeneralComponent implements OnInit {

  readonly URL_FRONT = environment.urlFront;
  readonly admin = 'BookEat';
  // public usuario: Usuario;
  
  public nombreUsuario: string;
  public flagUsuario: string = null;
  // public user: SocialUser;
  // public loggedIn: boolean;

  constructor(
    public usuarioService: UsuarioService,
    public restauranteService: RestauranteService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    if(localStorage.getItem("FLAG")){
      // If a restaurant its logged
      this.flagUsuario = localStorage.getItem("FLAG");

    }else{
      this.flagUsuario = null;
      // If an user its logged
      // this.authService.authState.subscribe((user) => {
      //   this.user = user;
      //   this.loggedIn = (user != null);
      // });
      // this.getUserByAccessToken();
    }
    this.nombreUsuario = localStorage.getItem("USER_NAME");
  }





  // Logout by form
  logout(): void{
    if(localStorage.getItem("FLAG")){
      // If a restaurant its logged
      console.log("hola")
      this.restauranteService.logoutRestaurante().subscribe( res => {
        if(res['status'] != 200){
          Swal.fire({
            title: 'Oops...', 
            html: 'Code: ' + res['status'] + '\n Message: ' + res['message'], 
            type: 'error'
          });
        }
      });
      this.flagUsuario = null;
      this.restauranteService.deleteTokenRestaurant();
    }else{
      // If an user its logged
      this.authService.signOut();
      // this.signOut();
      // this.usuarioService.selectedUsuario = new Usuario();
      this.usuarioService.logout().subscribe( res => {
        if(res['status'] != 200){
          Swal.fire({
            title: 'Oops...', 
            html: 'Code: ' + res['code'] + '<br> Message: ' + res['message'], 
            type: 'error'
          });
        }
      });
    }
    this.nombreUsuario = null;
    location.href = this.URL_FRONT + "/home";
  }




  // Logout with Facebook or google
  // signOut(): void {
  //   if(this.loggedIn){
  //     this.authService.signOut();
  //     this.loggedIn = false;
  //   }
  // }
}
