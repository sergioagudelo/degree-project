import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators, PatternValidator } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
// Packages used to work social media login
import { AuthService } from "angularx-social-login";
// From AuthService with social media we receive a SocialUser object
import { SocialUser } from "angularx-social-login";
// REV Agregar dominios en console.developers.google
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

// Created components
import { Usuario } from '../../models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { RestauranteService } from 'src/app/services/restaurante.service';

// Used to work with production or development enviroment
import { environment } from 'src/environments/environment';

// used to work emails
import { EmailService } from 'src/app/services/email.service';  


// Minimaps titles: interfaces deinition
// autocomplete events "event"
interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}






// Rev unico usuario y email para el registro
// Rev casos login y registrar
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  readonly URL_FRONT = environment.urlFront;
  // Register attrs
  public user: SocialUser;
  public loggedIn: boolean;
  // Real photo send to server
  file: File;
  // Photo as a ArrayBuffer
  photo: string | ArrayBuffer;

  constructor(
    // Rev error "ng build --prod --aot" cuando las clases son privadas
    public usuarioService: UsuarioService,
    public restauranteService: RestauranteService,
    public router: Router,
    public authService: AuthService,
    public email: EmailService
  ) {
    this.usuarioService.selectedUsuario = new Usuario();
   }

  ngOnInit() {
    // Thanks to the subscribe every time that the variable user its worked this method will be callNgModuleLifecycle, 
    // because the subscribe keep listening
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
  }

  // Resets modal form
  resetForm(form){
    if(form){
      form.reset();
      this.usuarioService.selectedUsuario = new Usuario();
    }
    this.photoSelected(null);
  }

  // Connect inputPhoto and imgPhoto and saves image on attr file
  photoSelected(event: HTMLInputEvent): void{
    // Check that there is a uploaded file
    if(event){
      if(event.target.files && event.target.files[0]){
        if(event.target.files[0].size < 2*1000*1000){
          // only files smaller than 2MB
          this.file = <File>event.target.files[0];
          // Image preview
          const reader = new FileReader();
          reader.onload = e => this.photo = reader.result;
          reader.readAsDataURL(this.file);
        }else{
          Swal.fire('Oops...', 'El archivo es demasiado grande. Asegurate de subir archivos de menos de 2MB!', 'error')
        }
      }
    }else{
      this.photo = 'assets/no-profile-photo.png';
    }
  }





  
  // minimap titles CRUD Users
  // Login an user and sent to home view
  onLogin(form): void {
    this.usuarioService.login(form.value).subscribe(res => {
      if(res['status'] == 200){
        if(res['usuario'].bloqueado == true ){
          Swal.fire({
            title: 'Oops...', 
            html: 'Su usuario ha sido bloqueado.',
            type: 'error'
          });
        }else{
          // Save token on localStorage
          this.usuarioService.saveToken(res['accessToken'], res['usuario'].nombre);          
          location.href = this.URL_FRONT + "/home";
          this.loggedIn = true;
        }
      }else{
        Swal.fire({
          title: 'Oops...', 
          html: res['message'],
          type: 'error'
        });
      }
    });
  }

  // Register an user and sent to home view
  onRegister(form): void {
    // We checked if both passwords match each other
    if(form.value.passwordRepeat == form.value.contrasena){
      this.usuarioService.register(form.value, this.file).subscribe(res => {
        // Hides the modal
        if(res['status'] == 200){
          let modalAddUser = <HTMLInputElement> document.getElementById('closeModalAddUser');
          modalAddUser.click();
          this.usuarioService.saveToken(res['accessToken'], res["usuario"].nombre);
          this.resetForm(form);
          location.href = this.URL_FRONT + "/home";
        }else{
          Swal.fire({
            title: 'Oops...', 
            html: 'Message: ' + res['message'],
            type: 'error'
          });
        }
      });
    }else{
      Swal.fire('Oops...', 'Las claves no coinciden!', 'error')
    }
  }
 
  // Minimap titles: login with social media
  // Login with Google
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    // Thanks to the subscribe every time that the variable user its worked this method will be callNgModuleLifecycle, 
    // because the subscribe keep listening
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      if(user){
        let userName = user.email.split("@gmail", 1);
        this.usuarioService.loginWithRedes(user, userName).subscribe(res => {
          if(this.loggedIn){
            location.href = this.URL_FRONT + "/home";
          }
        });
      }
    });
  }
 
  // Login with Facebook
  // signInWithFB(): void {
  //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  //   this.authService.authState.subscribe((user) => {
  //     this.usuarioService.loginWithRedes(user);
  //   });
  // } 
 
  // Logout with Facebook, google and facebook
  signOut(): void {
    if(this.loggedIn){
      this.authService.signOut();
      this.usuarioService.logout();
      this.loggedIn = false;
    }
  }





  // Minimap titles CRUD restaurantes
  // Login a restaurant and sent to admin profile restaurant view
  onLoginRestaurant(form): void {
    this.restauranteService.loginRestaurante(form.value).subscribe(res => {
      if(res["status"] == 200){
        // Rev change view to restaruant admin profile
        // Hides the modal
        // Save token on localStorage
        let modalRestaurant = <HTMLInputElement> document.getElementById('closeModalRestaurant');
        modalRestaurant.click();
        this.saveTokenRestaurant(res["accessToken"], res["restaurante"].nombre);
        location.href = this.URL_FRONT + "/view-profile-restaurant";
      }else{
        Swal.fire('Oooops...', res["message"], "error");
      }
    });
  }

  saveTokenRestaurant(accessToken: string, restaurante: string){
    localStorage.setItem("ACCESS_TOKEN", accessToken);
    localStorage.setItem("USER_NAME", restaurante);
    localStorage.setItem("FLAG", "restaurante");
  }



  // Generates aletory password when recovering account
  GenerateNewPassword() {
    let length = 10;
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

  // method used to recover password
  recoverPassword(form): void{
    let newPassword = this.GenerateNewPassword();
    this.usuarioService.getUserByEmailAndUpdatePassword(form.value.email, newPassword).subscribe((res) => {
      if(res['status'] == 200){
        this.email.sendEmailRecoverPassword(form.value.email, res['usuario']['nombre'], newPassword).subscribe((res) => {
          Swal.fire("Proceso exitoso",
          'Vas a recibir un mensaje en ' + form.value.email + ' si tu cuenta esta registrada con esta dirección e-mail.', "success");
      });
      }else {
        Swal.fire("Proceso fallido", res['message'], "error");
      }
    });
  }




  // minimap titles: validators
  // Rev if we want more secure validations, control maxlenght by pattern(regularExpression){0, maxlenght}
  // Validator for login User
  formLogin = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
    contrasena: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z0-9@.-<>]{5,20}/)])
  });

  // Validator for sing up User
  formUser = new FormGroup({
    usuario: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    correo: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
    fechaNacimiento: new FormControl(''),
    celular: new FormControl('', Validators.pattern(/^[0-9]{10}/)),
    contrasena: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z0-9@.-<>]{5,20}/)]),
    passwordRepeat: new FormControl('', Validators.required)
  });

  // Validator for login User
  formLoginRest = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
    contrasena: new FormControl('', [Validators.required, Validators.pattern(/^[A-Za-z0-9@.-<>]{5,20}/)])
  });

  // minimap titles: validators
  recoverPasswordForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
  });
}