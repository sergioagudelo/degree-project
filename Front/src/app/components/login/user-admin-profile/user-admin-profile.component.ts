import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js'

import { UsuarioService } from 'src/app/services/usuario.service';
import { Usuario } from '../../../models/usuario';

// Used to work with production or development enviroment
import { environment } from 'src/environments/environment';



// Minimaps titles: interfaces deinition
// USed to work with files
interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}






@Component({
  selector: 'app-user-admin-profile',
  templateUrl: './user-admin-profile.component.html',
  styleUrls: ['./user-admin-profile.component.css']
})
export class UserAdminProfileComponent implements OnInit {

  // Rev control inputdate like this
  // readonly maxInputDate = new Date().getFullYear() + "-" + new Date().getMonth() + "-" + new Date().getDate();
  // readonly minInputDate = new Date().getFullYear() - 100 + "-" + new Date().getMonth() + "-" + new Date().getDate();

  readonly URL_FRONT = environment.urlFront;
  readonly URL_IMGS = environment.urlImgs + "usuarios/";

  public accessToken: string = this.usuarioService.getToken();
  // used to change password
  public currentPassword: string;
  public newPassword: string;
  public passwordConfirm: string;
  // Real photo
  file: File;
  // Photo as a ArrayBuffer
  photo: string | ArrayBuffer;
  // User´s actual photo
  fotoActual: string = 'assets/no-profile-photo.png';

  constructor(
    public usuarioService: UsuarioService,
    public router: Router
  ) {   
  }
  
  

  ngOnInit() {
    if(localStorage.getItem("ACCESS_TOKEN")){
      this.getUser();
    }else{
      // If there aren´t a setted token
      Swal.fire('Ha ocurrido un error con su sesión.', 'por favor ingrese nuevamente.', 'error');
      // We set selectedUsuario as a new Usuario because methods that use selectedUsuario doesn´t work with null´s elements
      this.usuarioService.selectedUsuario = new Usuario();
      this.router.navigateByUrl('/login');
    }
  }




  // Minimap titles: user-admin-profile funcions
  // Resets modal from
  resetForm(form?: NgForm){
    if(form){
      form.reset();
    }
    this.photoSelected(null);
  }

  // Connect inputPhoto and imgPhoto and saves image on attr file
  photoSelected(event: HTMLInputEvent): void{
    // Check that there is a uploaded file
    if(event){
      if(event.target.files && event.target.files[0]){
        if(event.target.files[0].size < 2*1000*1000){
          // only files smaller than 2MB
          this.file = <File>event.target.files[0];
          // Image preview
          const reader = new FileReader();
          reader.onload = e => this.photo = reader.result;
          reader.readAsDataURL(this.file);
        }else{
          Swal.fire('Oops...', 'El archivo es demasiado grande. Asegurate de subir archivos de menos de 2MB!', 'error')
        }
      }
    }else{
      this.photo = 'assets/no-profile-photo.png';
    }
  }





  // Minimap titles: CRUD users
  // Method useed to get an user by his accessToken
  getUser(){
    this.usuarioService.getUserByAccessToken(this.accessToken)
    .subscribe(res => {
      if(res['status'] != 200){
        // If the accessToken isn´t well defined
        Swal.fire({
          title: 'Oops...', 
          html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
          '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
          type: 'error',
          confirmButtonText: 'OK'
        }).then((result) => {
          if (result.value) {
            location.href = this.URL_FRONT + "/login";
          }
        });
        this.usuarioService.selectedUsuario = new Usuario();
        // location.href = URL_FRONT + "/login";
        this.usuarioService.deleteToken("ACCESS_TOKEN", "USER_NAME");
      }else{
        // If everything its okay
        this.usuarioService.selectedUsuario = res['usuario'] as Usuario;
        // for multer
        // this.fotoActual = this.URL_API + "/" + this.usuarioService.selectedUsuario.fotoPerfil;
        // for aws buckets
        if(this.usuarioService.selectedUsuario.fotoPerfil &&
          this.usuarioService.selectedUsuario.fotoPerfil != 'assets/no-profile-photo.png'){
          this.fotoActual = this.URL_IMGS + this.usuarioService.selectedUsuario.fotoPerfil;
        }
      }
    });
  }

  // Update an user
  updateUser(form?: NgForm){
    const usuario = {
      _id: this.usuarioService.selectedUsuario._id,
      accessToken: this.usuarioService.getToken(),
      fotoPerfil: this.usuarioService.selectedUsuario.fotoPerfil,
      usuario: form.value.usuario,
      nombreUsuario: form.value.nombreUsuario,
      correo: form.value.correo,
      fechaNacimiento: form.value.fechaNacimiento,
      celular: form.value.celular,
      contrasenaUser: this.usuarioService.selectedUsuario.contrasena
    };
    this.usuarioService.putUsuario(usuario, this.file)
      .subscribe(res => {
        if(res['status'] == 200){
          Swal.fire(res['message'], 'Proceso Terminado.', 'success');
        }else{
          Swal.fire({
            title: 'Oops...', 
            html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
            '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
            type: 'error',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.value) {
              this.usuarioService.deleteToken('ACCESS_TOKEN', 'USER_NAME');
              location.href = this.URL_FRONT + "/login";
            }
          });
        }
      });
  }

  // Changes password
  changePassword(form?: NgForm){
    if(this.newPassword == this.passwordConfirm){
      this.usuarioService.changePassword(form.value._id, this.currentPassword, this.newPassword).subscribe(res =>{
        if(res['status'] == 200){
          Swal.fire('Clave cambiada.', res["message"], 'success');
          let closeModalAddUser = <HTMLInputElement> document.getElementById('closeModalCambiarClave');
          closeModalAddUser.click();
        }else{
          Swal.fire('Ooops...', res["message"], 'error');
        }
      });
    }else{
      Swal.fire('Confirmar nueva contraseña', 'Las claves no coinciden.', 'error');
    }
  }
}
