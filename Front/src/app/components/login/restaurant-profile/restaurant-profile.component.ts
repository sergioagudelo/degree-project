import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgForm } from '@angular/forms';


import { Restaurante } from '../../../models/restaurante';
import { RestauranteService } from 'src/app/services/restaurante.service';
import { environment } from 'src/environments/environment';
import { platformCoreDynamicTesting } from '@angular/platform-browser-dynamic/testing/src/platform_core_dynamic_testing';
import { SelectorContext } from '@angular/compiler';




// Minimaps titles: interfaces deinition
// USed to work with files, sets the kind of event as evenrTarger and inputElement
interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}





@Component({
  selector: 'app-restaurant-profile',
  templateUrl: './restaurant-profile.component.html',
  styleUrls: ['./restaurant-profile.component.css']
})
export class RestaurantProfileComponent implements OnInit {
  readonly URL_FRONT = environment.urlFront;
  readonly URL_API = environment.urlBack;
  readonly URL_IMGS_LOCAL = environment.urlImgs + 'restaurants/locals/';
  readonly URL_IMGS_LOGO = environment.urlImgs + 'restaurants/logos/';
  readonly URL_IMGS_PLATE = environment.urlImgs + 'restaurants/plates/';

  public accessToken: string = localStorage.getItem("ACCESS_TOKEN");
  public id: string;
  // used to change password
  public currentPassword: string;
  public newPassword: string;
  public passwordConfirm: string;
  // Real photo
  fileLocal: File;
  fileLogo: File;
  filePlate: File;
  // Photo as a ArrayBuffer
  photo: string | ArrayBuffer;  
  photoLocal: string | ArrayBuffer;  
  photoLogo: string | ArrayBuffer;  
  photoLocalActual: string = 'assets/no-profile-photo.png';
  photoLogoActual: string = 'assets/no-profile-photo.png';
  // arrays for horario, categoria, servicio, metodopago
  horario = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'];
  categorias;
  // categorias = [
  //   'Cómida rápida', 'Cómida Gourmet', 'Cómida Parrilla','Cómida Dulce', 'Bebida Fría', 'Brunch',
  //   'Bebida Caliente', 'Ensaladas', 'Almuerzos', 'Cómida Italiana', 'Cómida De Mar', 'Cómida Saludable'
  // ];
  servicios = [
    'Baño','WiFi','Domicilios','Aire acondicionado', 'Zona de juegos infantiles', 'Pet friendly',
    'Reservar de fechas especiales para eventos de picnic', 'Eventos', 'Reposteria para Eventos', 'Catering',
    'Chef en Casa'
  ];
  metodosPago = ['Efectivo','Tarjeta de crédito','Tarjeta débito', 'Transferencias'];
  
  constructor(
    public restauranteService: RestauranteService,
    public router: Router
  ) { }

  ngOnInit() {
    if(this.accessToken){
      this.getRestaurant();
    }else{
      // If there aren´t a setted token
      Swal.fire('Ha ocurrido un error con su sesión.', 'por favor ingrese nuevamente.', 'error');
      // We set selectedRestaurante as a new Restaurante because methods that use selectedRestaurante doesn´t work with null´s elements
      this.restauranteService.selectedRestaurante = new Restaurante();
      this.router.navigateByUrl('/login');
    }
    // this.photoLocal = this.restauranteService.selectedRestaurante.fotoLocal;
    // this.photoLogo = this.restauranteService.selectedRestaurante.logo;
  }




  // Minimap titles: user-admin-profile funcions
  // Resets modal from
  resetForm(form?: NgForm){
    if(form){
      console.log(form)
      form.reset();
    }
    this.photo = null;
  }

  // Connect inputPhoto and imgPhoto and saves image on attr file
  // method for plates, local and logo
  photoSelected(event: HTMLInputEvent, whichPhoto?: string): void{
    // Check that there is a uploaded file
    if(event){
      if(event.target.files && event.target.files[0]){
        if(event.target.files[0].size < 2*1000*1000){
          // only files smaller than 2MB
          // Image preview
          const reader = new FileReader();
          if(whichPhoto == 'local'){
            // if worked photo local
            this.fileLocal = <File>event.target.files[0];
            reader.onload = e => this.photoLocal = reader.result;
            reader.readAsDataURL(this.fileLocal);
          }
          else if(whichPhoto == 'logo'){
            // if worked photo logo
            this.fileLogo = <File>event.target.files[0];
            reader.onload = e => this.photoLogo = reader.result;
            reader.readAsDataURL(this.fileLogo);
          }
          else{
            // if worked photo plate
            this.filePlate = <File>event.target.files[0];
            reader.onload = e => this.photo = reader.result;
            reader.readAsDataURL(this.filePlate);
          }
        }else{
          Swal.fire('Oops...', 'El archivo es demasiado grande. Asegurate de subir archivos de menos de 2MB!', 'error')
        }
      }  
    }
  }

  // Check services, paidmethods and categorys
  checkCheckbox(services, categorys, paidMethods){
    // create array categorys
    // if(categorys)
    //   for(let categoria of categorys){
    //     if(this.categorias.includes(categoria)){
    //       let category = <HTMLInputElement> document.getElementById(categoria + "-Restaurante");
    //       category.checked = true;
    //     }
    //   }
    // create array services
    if(services)
      for(let servicio of services){
        if(this.servicios.includes(servicio)){
          let service = <HTMLInputElement> document.getElementById(servicio);
          service.checked = true;
        }
      }
    // create array paidMethods
    if(paidMethods)
      for(let metodopago of paidMethods){
        if(this.metodosPago.includes(metodopago)){
          let paidMethod = <HTMLInputElement> document.getElementById(metodopago);
          paidMethod.checked = true;
        }
      }
  }

  // Sets scheduled to be printed and print it on DOM
  splitAndPrintSchedule(schedule){
    let scheduleDay = [];
    let i = 0;
    for(let dia of this.horario){
      if(schedule.length > 0){
        scheduleDay.push(schedule[0][dia].split(/[ :-]+/));
        if(scheduleDay[i] != 'cerrado'){
          let horaAbre = <HTMLInputElement> document.getElementById(dia + 'horaAbre');
          horaAbre.value= scheduleDay[i][0];
          let minutoAbre = <HTMLInputElement> document.getElementById(dia + 'minutoAbre');
          minutoAbre.value = scheduleDay[i][1];
          let horaCierra = <HTMLInputElement> document.getElementById(dia + 'horaCierra');
          horaCierra.value = scheduleDay[i][2];
          let minutoCierra = <HTMLInputElement> document.getElementById(dia + 'minutoCierra');
          minutoCierra.value = scheduleDay[i][3];
        }
      }
      i++;
    }
  }






  // minimap titles: CRUD restaurant
  // Method used to get an restaurant by his accessToken
  getRestaurant(){
    this.restauranteService.getRestaurantByToken(this.accessToken)
    .subscribe(res => {
      if(res['status'] == 200){
        this.restauranteService.selectedRestaurante = res['restaurant'] as Restaurante;
        // if the restaurant exists
        this.id = this.restauranteService.selectedRestaurante._id;
        // check if this.photoLocalActual and this.photoLogoActual exists
        if(this.restauranteService.selectedRestaurante.fotoLocal){
          this.photoLocalActual = this.URL_IMGS_LOCAL + this.restauranteService.selectedRestaurante.fotoLocal;
        }
        if(this.restauranteService.selectedRestaurante.logo){
          this.photoLogoActual = this.URL_IMGS_LOGO + this.restauranteService.selectedRestaurante.logo;
        }
        this.categorias = this.restauranteService.selectedRestaurante.categoria;
        // checks checkbox
        this.checkCheckbox(
          this.restauranteService.selectedRestaurante.servicio,
          this.restauranteService.selectedRestaurante.categoria,
          this.restauranteService.selectedRestaurante.metodoPago
        );
        this.splitAndPrintSchedule(this.restauranteService.selectedRestaurante.horario);
      }else{
        // If the accessToken isn´t well defined
        Swal.fire({
          title: 'Oops...', 
          html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
          '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
          type: 'error',
          confirmButtonText: 'OK'
        }).then((result) => {
          if (result.value) {
            location.href = this.URL_FRONT + "/login";
          }
        });
        // We set selectedRestaurante as a new Restaurante because methods that use selectedRestaurante doesn´t work with null´s elements
        this.restauranteService.selectedRestaurante = new Restaurante();
        localStorage.removeItem("FLAG");
        localStorage.removeItem("USER_NAME");
        localStorage.removeItem("ACCESS_TOKEN");
      }
    });
  }



  // Update a restaurant
  updateRestaurant(form?: NgForm){
    // create array categorys
    this.restauranteService.selectedRestaurante.categoria = [];
    for(let categoria of this.categorias){
      let category = <HTMLInputElement> document.getElementById(categoria + "-Restaurante");
      if(category.checked){
        this.restauranteService.selectedRestaurante.categoria.push(categoria)
      }
    }
    // create array services
    this.restauranteService.selectedRestaurante.servicio = [];
    for(let servicio of this.servicios){
      let service = <HTMLInputElement> document.getElementById(servicio);
      if(service.checked){
        this.restauranteService.selectedRestaurante.servicio.push(servicio)
      }
    }
    // create array paidMethods
    this.restauranteService.selectedRestaurante.metodoPago = [];
    for(let metodoago of this.metodosPago){
      let paidMethod = <HTMLInputElement> document.getElementById(metodoago);
      if(paidMethod.checked){
        this.restauranteService.selectedRestaurante.metodoPago.push(metodoago)
      }
    }
    // sets horario
    var schedule = {};
    let wellDefined = true;
    for(let dia of this.horario){
      let horaAbre = <HTMLInputElement> document.getElementById(dia + 'horaAbre');
      let minutoAbre = <HTMLInputElement> document.getElementById(dia + 'minutoAbre');
      let horaCierra = <HTMLInputElement> document.getElementById(dia + 'horaCierra');
      let minutoCierra = <HTMLInputElement> document.getElementById(dia + 'minutoCierra');
      if(!horaAbre.value || !minutoAbre.value || !horaCierra.value || !minutoCierra.value){
        // closed that day
        schedule[dia] = 'cerrado';
      }else if(0 <= +horaAbre.value && +horaAbre.value <= 23 && 0 <= +minutoAbre.value && +minutoAbre.value <= 59
        &&
        0 <= +horaCierra.value && +horaCierra.value <= 23 && 0 <= +minutoCierra.value && +minutoCierra.value <= 59){
        // schedule well defined
        schedule[dia] = this.setPatternTime(horaAbre.value, minutoAbre.value, horaCierra.value, minutoCierra.value);
        console.log(schedule[dia])
      }else{
        // The hour or minutes were not well defined
        Swal.fire('El horario para el dia: ' + dia + ' esta mal definido.<br>' + 
        '(' + horaAbre.value + ':' + minutoAbre.value + ' - ' + horaCierra.value + ':' + minutoCierra.value + ')', 'error', 'error');
        wellDefined = false;
      }
    }

    if(wellDefined){
      Swal.fire({
        title: 'Horario',
        html: 'Su horario de atención a comensales sera establecido como:<br>' +
        'Lunes: ' + schedule['Lunes'] + '<br>' +
        'Martes: ' + schedule['Martes'] + '<br>' +
        'Miércoles: ' + schedule['Miércoles'] + '<br>' +
        'Jueves: ' + schedule['Jueves'] + '<br>' +
        'Viernes: ' + schedule['Viernes'] + '<br>' +
        'Sábado: ' + schedule['Sábado'] + '<br>' +
        'Domingo: ' + schedule['Domingo'] + '<br>',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Correcto!',
        cancelButtonText: 'Incorrecto'
      }).then((result) => {
        if (result.value) {
          this.restauranteService.selectedRestaurante.horario = schedule;
          // By this way we make sure that the accessToken it a valid one
          this.restauranteService.selectedRestaurante.accessToken = localStorage.getItem("ACCESS_TOKEN");
          this.restauranteService.putRestaurante(this.restauranteService.selectedRestaurante)
            .subscribe(res => {
              if(res['status'] == 200){
                Swal.fire(res['message'], 'Exito', 'success');
              }else{
                Swal.fire({
                  title: 'Oops...', 
                  html: 'Code: ' + res['status'] + '\n Message: ' + res['message'] +
                  '<br>¿Ha ocurrido un error con su sesión, por favor ingrese nuevamente.',
                  type: 'error',
                  confirmButtonText: 'OK'
                }).then((result) => {
                  if (result.value) {
                    location.href = this.URL_FRONT + "/login";
                  }
                });
                // We set selectedRestaurante as a new Restaurante because methods that use selectedRestaurante doesn´t work with null´s elements
                this.restauranteService.selectedRestaurante = new Restaurante();
                localStorage.removeItem("FLAG");
                localStorage.removeItem("USER_NAME");
                localStorage.removeItem("ACCESS_TOKEN");
              }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire(
            'Cancelado',
            'Los datos no se han actualizado',
            'error'
          )
        }
      });
    }
  }





  // sets pattern time when time is less than 10 to 0x where x its the seleted time. ej: 1 to 01
  // {scheduleDay} returned value
  setPatternTime(horaAbre: String, minutoAbre: String, horaCierra: String, minutoCierra: String){
    // returned value
    let scheduleDay: String = "";
    // array with times to be evaluated
    let arrayHours: String[] = [horaAbre, minutoAbre, horaCierra, minutoCierra];

    let indice = 0;
    // verifies element by element the pattern time
    arrayHours.forEach(selectedHour => {
      if(Number(selectedHour) < 10 && selectedHour.length < 2){
        // if hour its less dan 10 (1, 2, ..., x, ..., 9) sets it as 0x by combetion
        arrayHours[indice] = "0" + selectedHour;
      }
      indice++;
    });

    scheduleDay = arrayHours[0] + ':' + arrayHours[1] + ' - ' + arrayHours[2] + ':' + arrayHours[3];
    return scheduleDay;
  }





  // creates local and logo images
  changePhotoLocalLogo(form?: NgForm){
    let fail = false;

    if(this.fileLocal || this.fileLogo){
      if(this.fileLocal){
        this.restauranteService.changePhotoLocal(this.id, this.fileLocal)
        .subscribe(res => {
          if(res['status'] != 200){
            fail = true;
          }
        });
      }
      if(this.fileLogo){
        this.restauranteService.changePhotoLogo(this.id, this.fileLogo)
        .subscribe(res => {
          if(res['status'] != 200){
            fail = true;
          }
        });
      }
      if(fail){
        Swal.fire('Ooops...', 'Ocurrio un problema al cargar las imágenes.\n' +
        'vuelva a intentarlo', 'error');
      }else{
        Swal.fire('Fotos actualizadas.', 'Exito', 'success');
      }
    }else{
      Swal.fire('No ha seleccionado ninguna imagen.', 'info');
    }
  }

  // Creates a plate for an specific restaurant
  createPlate(form: NgForm){
    // checks categorys for created plates
    if(this.photo){
      // create array with checked categorys plate
      // {plateCategorys} saves categories for specific plates
      // also
      // looks into plateCategorys for new categories on the restaurant and sent the new array categories
      // {restaurantCategories} new array categories
      let restaurantCategories = this.restauranteService.selectedRestaurante.categoria;
      let plateCategorys = [];
      for(let categoria of this.categorias){
        let category = <HTMLInputElement> document.getElementById(categoria + "-Plato");
        if(category.checked){
          // if plate its catetegory
          plateCategorys.push(categoria);
          if(!restaurantCategories.includes(categoria)){
            // if categoria its a new restaurant category add the new category
            restaurantCategories[restaurantCategories.length] = categoria;
          }
        }
      }

      // create array with checked days promo plate
      let plateDayProms = [];
      for(let dia of this.horario){
        let dayProm = <HTMLInputElement> document.getElementById(dia + "-Plato");
        if(dayProm.checked){
          plateDayProms.push(dia);
        }
      }

      const plato = {
        nombre: form.value.nombre,
        categoria: JSON.stringify(plateCategorys),
        descripcion: form.value.descripcion,
        precio: form.value.precio,
        diaPromocion: JSON.stringify(plateDayProms)
      };
      console.log(plato)
      this.restauranteService.createPlate(this.id, plato, this.filePlate, JSON.stringify(restaurantCategories))
        .subscribe( res => {
          if(res){
            Swal.fire('Plato creado.', 'Plato ' + plato.nombre + ' creado satisfactoriamente', 'success');
            let buttonCloseModalPlate = <HTMLElement> document.getElementById('closeModalPlate') ;
            buttonCloseModalPlate.click();
            this.getRestaurant()
            this.resetForm(form);
          }
        }, err => {
          Swal.fire(err, 'El plato no se pudo crear', 'error');
        });
    }
    else{
      Swal.fire('Debe ingresar una foto del plato.', 'error', 'error');
    }
  }

  // Delete a plate
  // Rev improve the way on which we delete platformCoreDynamicTesting, its better if we use a combination of price and foto
  deletePlate(foto: string){
    Swal.fire({
      title: '¿Desea eliminar el plato?',
      text: 'Se eliminara el plato si presiona en "Si, Eliminar',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      // Deletes the plate
      if (result.value) {
        // Rev check if with a deleted plate restaurant categories needs to be update because a non-existen category
        this.restauranteService.deletePlate(this.id, foto, )
          .subscribe( res => {
            if(res){
              Swal.fire('Plato eliminado.', 'El plato se ha eliminado exitosamente.', 'success');
              this.getRestaurant();
            }
          }, err => {
            Swal.fire('err', 'error', 'error');
          });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Dosen´t delete the plate
        Swal.fire(
          'Cancelado',
          'El plato no se ha eliminado.',
          'error'
        )
      }
    })
  }

  // changes password
  changePassword(form?: NgForm){
    if(this.newPassword == this.passwordConfirm){
      this.restauranteService.changePassword(this.accessToken, this.id, this.currentPassword, this.newPassword).subscribe(res =>{
        if(res['status'] == 200){
          Swal.fire(res["message"],'Exito', 'success');
          let closeModalAddUser = <HTMLInputElement> document.getElementById('closeModalCambiarClave');
          closeModalAddUser.click();
        }else{
          Swal.fire('Ooops...', res['message'], 'error');
        }
      });
    }else{
      Swal.fire('Confirmar nueva contraseña.', 'Las claves no coinciden.', 'error');
    }
  }

}