// Imports of angular componentes and packages
import { Component, OnInit, ɵConsole } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators, FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';

// Imports of created models
import { Usuario } from '../../models/usuario';
import { Restaurante } from '../../models/restaurante';
import { Resena } from 'src/app/models/resena';

// Imports of created services
import { UsuarioService } from '../../services/usuario.service';
import { RestauranteService } from '../../services/restaurante.service';
import { ResenaService } from 'src/app/services/resena.service';
import { environment } from 'src/environments/environment';
import { ɵangular_packages_platform_browser_dynamic_platform_browser_dynamic_a } from '@angular/platform-browser-dynamic';







@Component({
  selector: 'app-view-admin',
  templateUrl: './view-admin.component.html',
  styleUrls: ['./view-admin.component.css'],
  providers: [UsuarioService]
})
export class ViewAdminComponent implements OnInit {
  readonly URL_API = environment.urlBack;
  readonly URL_IMGS_USER = environment.urlImgs + 'usuarios/';
  // These propeerties are used to manage the tables
  public boolTableUserRestRes: any = true;
  public thUser: Array<String>;
  public thRest: Array<String>;
  public thResena: Array<String>;
  // dataResenas its used to set nombre restaurants by id on resenas
  public dataResenas = [];

  // Atributes used to make the filtered search
  public userSearch: String;
  public nombreSearch: String;
  public emailSearch: String;
  public valueSearch: String = "";
  public inputSelected: number;

  // Attributes used when we register users
  public passwordConfirm: String = null;

  constructor(
    public usuarioService: UsuarioService,
    public restauranteService: RestauranteService,
    public resenaService: ResenaService
    ) {
    this.thUser = ['#', 'Foto', 'Usuario', 'Nombre', 'Correo', 'F. Nacimiento', 'Celular', 'Acción'];
    this.thRest = ['#', 'Nombre', 'Direccion', 'Telefono', 'P. Minimio', 'Categoria', 'Acción'];
    this.thResena = ['#', 'Restaurante', 'Usuario', 'Texto', 'Calificación', 'Acción'];
  }

  ngOnInit() {
    this.getUsers();
    this.getRestaurants();
  }





  // minimap titles Admin view functions
  // Search users, restaurants and manage the table
  manageTableUserRest(boolTableUserRestRes) {
    this.boolTableUserRestRes = boolTableUserRestRes;
    // if(boolTableUserRestRes == true){
    //   this.getUsers();
    // }else if(boolTableUserRestRes == false){
    //   this.getRestaurants();
    // }
    // its mandatory to call this.getResenas() only when the user requires it, otherwise there will be a prcd
    if(boolTableUserRestRes == 2){
      this.getResenas();
    }
  }

  // cleans filters values
  cleanFilters(cleanedFilter?: number){
    console.log(typeof cleanedFilter)
    if(cleanedFilter == 1){
      this.userSearch = "";
      this.nombreSearch = "";
      this.emailSearch = "";
    }else{
      this.valueSearch = "";
    }
  }
  // Resets modal from
  resetForm(form?: NgForm){
    if(form){
      form.reset();
      this.usuarioService.selectedUsuario = new Usuario();
    }
    this.passwordConfirm = null;
  }

  cleanModalUserRest(){
    // if(this.usuarioService.selectedUsuario._id){
      this.usuarioService.selectedUsuario = new Usuario();
    // }else if(this.restauranteService.selectedRestaurante._id){
      // Rev Returns an error
      // this.restauranteService.selectedRestaurante = new Restaurante();
    // }
    this.passwordConfirm = null;
  }

  // Disable the inputs that aren't used to make de filtered table
  selectedInput(selectedInput?: number){
    this.inputSelected = selectedInput;
    if(selectedInput == 1){
      // If selected input user
      this.nombreSearch = "";
      this.emailSearch = "";
    }else if(selectedInput == 2){
      // If selected input name
      this.userSearch = "";
      this.emailSearch = "";
    }else if(selectedInput == 3){
      // If selected input email
      this.userSearch = "";
      this.nombreSearch = "";
    }
  }





  // minimap titles CRUD Users
  // Add or Update an user
  addUser(form?: NgForm){
    // If an ID exists then the method will update and user otherwise the method will create a new user
    if(form.value._id){
      // Rev to properly check if both passwords are the same we also need to check if the admin updates de actual password
      // If he chages actual password and left null the attr confirm password then the rest will be update
      if(this.passwordConfirm != null && this.passwordConfirm != form.value.contrasenaUser){
        Swal.fire('Las contraseñas no conciden.', "error", "error");
      }else{
        this.usuarioService.putUsuario(form.value, null, this.usuarioService.getToken())
        .subscribe(res => {
          if(res['status'] == 200){
            Swal.fire('Usuario ' + form.value.nombreUsuario + ' Guardado Satisfactoriamente.', res['message'], "success");
            this.resetForm(form);
            this.getUsers();
          }else{
            Swal.fire('Ooops.', res['message'], "error");
          }
        });
      }
    }else {
      if(this.passwordConfirm == form.value.contrasenaUser){
        this.usuarioService.postUsuario(form.value)
        // subscribe is the way to listen answer of the server
        .subscribe(res => {
          Swal.fire('Usuario ' + form.value.nombreUsuario + ' Guardado Satisfactoriamente.', "Proceso Terminado", "success");
          this.resetForm(form);
          this.getUsers();
        });
      }else{
        Swal.fire('Las contraseñas no conciden.', "error", "error");
      }
    }
  }

  // Gets all the users
  getUsers(){
    this.userSearch = "";
    this.nombreSearch = "";
    this.emailSearch = "";
    this.usuarioService.getUsuarios()
    .subscribe(res => {
      this.usuarioService.usuarios = res as Usuario[];
    })
  }

  // Sets selectedUsuario as user to work in modalAddUser
  editUser(user: Usuario){    
    this.usuarioService.selectedUsuario = user;
  }

  // Delete an user
  deleteUser(_id: String, nombre: String){
    Swal.fire({
      title: '¿Estás seguro?',
      text: '¿Estás seguro de eliminar a ' + nombre + '?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.usuarioService.deleteUsuario(_id)
          .subscribe(res => {
            this.getUsers();
            // console.log(res);
            Swal.fire('Usuario ' + nombre + ' Eliminado Satisfactoriamente', "Exito", "success");
          });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'No se ha completado la acción, el usuario no se elimino',
          'error'
        )
      }
    });  
  }

  // Returns a filtered array used to show an specific user
  getFilteredUsers(){
    if(this.inputSelected == 1){
      // If selected input user
      this.valueSearch = this.userSearch;
    }else if(this.inputSelected == 2){
      // If selected input name
      this.valueSearch = this.nombreSearch;
    }else if(this.inputSelected == 3){
      // If selected input email
      this.valueSearch = this.emailSearch;
    }

    // Validates button buscar its correctly used, we can use only one input per filtered search
    if(this.valueSearch == ""){
      this.manageTableUserRest(true);
    }else{
      this.usuarioService.getFilteredUsers(this.valueSearch, this.inputSelected)
      .subscribe(res => {
        if(res["status"]){
          // The user doesn´t exists
          Swal.fire(res["status"], "Error", "error");
        }else{
          // The user exists
          this.usuarioService.usuarios = res as Usuario[];
        }
      })
    }
  }

  // Update an sepecific attribute of user
  updateEstadoUser(userId: String){
    let estado = document.getElementById('checkboxEstado' + userId) as HTMLInputElement;
    let value:Boolean = estado.checked;
    this.usuarioService.updateCampo(userId, 'bloqueado', value)
      .subscribe(res => {
        console.log(res);
    });

    // Used to manage background based on estado
    let tr = document.getElementById('tr' + userId) as HTMLInputElement;
    if(value){
      tr.style.background = '#FFCDC2';
    }else{
      tr.style.background = '';
    }
  }





  // Minimap titles CRUD Restaurants
  // Add or Update a rastaurant
  addRest(form?: NgForm){
    console.log(form.value.categoria)
    console.log(form.value.categoria)
    if(form.value.categoria.split(",")){
      form.value.categoria = form.value.categoria.split(",");
    }
    console.log(form.value.categoria)
    // If an id exist the restaurant will be updated otherwise will be created
    if(form.value._id){
      // Rev to properly check if both passwords are the same we also need to check if the admin updates de actual password
      // If he chages actual password and left null the attr confirm password then the rest will be update
      if(this.passwordConfirm != null && this.passwordConfirm != form.value.contrasenaRest){
        Swal.fire('Las contraseñas no conciden.', "Error", "error");
      }else{
        this.restauranteService.putRestauranteFromViewAdmin(form.value, localStorage.getItem("ACCESS_TOKEN"))
          .subscribe(res => {
            if(res['status'] == 200){
              Swal.fire('Restaurante ' + form.value.nombre + ' Actualizado Satisfactoriamente.', 'Guardado', "success");
              this.resetForm(form);
              this.getRestaurants();
            }else{
              Swal.fire('Ooops.', res['message'], "error");
            }
          });
      }
    }else{
      if(this.passwordConfirm != form.value.contrasenaRest){
        Swal.fire('Las contraseñas no conciden.', "Error", "error");
      }else{
        this.restauranteService.postRestaurante(form.value)
          .subscribe(res => {
            Swal.fire('Restaurant ' + form.value.nombre + ' saved successfuly.', 'Saved', "success");
            this.resetForm(form);
            this.getRestaurants();
          });
      }
    }
  }

  // Gets all the restaurants, even those blocked
  getRestaurants(){
    this.nombreSearch = "";
    this.restauranteService.getAllRestaurants()
      .subscribe(res => {
        this.restauranteService.restaurantes = res as Restaurante[];
      })
  }

  // Sets selectedRestaurant as restaurant to work in modalAddRestaurant
  editRest(restaurant: Restaurante){
    this.restauranteService.selectedRestaurante = restaurant;
  }

  // Delete a restaurant
  deleteRest(_id: string, nombre: String){
    Swal.fire({
      title: '¿Estáas seguro?',
      text: '¿Estás seguro de eliminar a ' + nombre + '?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        this.restauranteService.deleteRestaurante(_id)
          .subscribe(res => {
            this.getRestaurants();
            // console.log(res);
            Swal.fire('Restaurante ' + nombre + ' Eliminado Satisfactoriamente.', "Exito", "success");
          })
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'No se ha completado la acción, el usuario no se elimino',
          'error'
        )
      }
    });
  }

  // Returns a filtered array used to show an specific restaurant
  getFilteredRestaurants( ){
    if(this.valueSearch == ""){
      this.manageTableUserRest(false);
    }else{
      this.restauranteService.getFilteredRestaurants(this.valueSearch)
      .subscribe(res => {
        if(res["status"] !=undefined){
          // The restaurant doesn´t exists
          Swal.fire(res["status"], "Error", "error");
        }else{
          // The restaurant exists
          this.restauranteService.restaurantes = res as Restaurante[];
        }
      })
    }
  }

  // Update an sepecific attribute of restaurant
  updateEstadoRest(restId: String){
    let estado = document.getElementById('checkboxEstado' + restId) as HTMLInputElement;
    let value:Boolean = estado.checked;
    this.restauranteService.updateCampo(restId, 'bloqueado', value)
      .subscribe(res => {
        console.log('Restaurant ' + restId + ' blocked/unblocked successfuly');
    });

    // Used to manage background based on estado
    let tr = document.getElementById('tr' + restId) as HTMLInputElement;
    if(value){
      tr.style.background = '#FFCDC2';
    }else{
      tr.style.background = '';
    }
  }





  // Minimap titles: CRUD resenas
  // Get resenas
  getResenas() {
    this.resenaService.getResenas()
      .subscribe(res => {
        this.resenaService.resenas = res as Resena[];
        this.getNameRestaurantsById();
      });
  }
  // method used to charge restaurants name based on ID to show on table reseñas
  getNameRestaurantsById(){
    let settedResenas = {};
    this.dataResenas = [];
    let i = 0;
    console.log(this.resenaService.resenas)
    console.log(this.restauranteService.restaurantes)
    // creates object resenas
    this.restauranteService.restaurantes.forEach(restaurant => {
      this.resenaService.resenas.forEach(resena => {
        if(restaurant._id == resena.restaurante){
          // sets reseña with restaurante name
          Object.assign(settedResenas, {[i]: {
            _id: resena._id,
            idRestaurante: resena.restaurante,
            nameRestaurant: restaurant.nombre,
            usuario: resena.usuario,
            resena: resena.texto,
            calificacion: resena.calificacion
          }}, {});
          i++;
        }
      });
    });

    // creates array resenas to work with *ngFor
    for(var resena in settedResenas){
      this.dataResenas.push(settedResenas[resena]);
    }
    console.log(settedResenas)
  }

  // Delete a resena
  deleteResena(_id: string){
    if(confirm('¿Está seguro de eliminar la reseña con ID: ' + _id + '?')){
      this.resenaService.deleteResena(_id)
        .subscribe(res => {
          console.log(res)
          this.getResenas();
          Swal.fire('Reseña ' + _id + ' Eliminada Satisfactoriamente.', "Exito", "success");
        })
    }
  }

  // Returns a filtered array of resenas
  getFilteredResenas(){
    if(this.valueSearch == ""){
      this.manageTableUserRest(2);
    }else{
      this.resenaService.getFilteredResenas(this.valueSearch)
      .subscribe(res => {
        if(res["status"] !=undefined){
          // The restaurant doesn´t exists
          Swal.fire(res["status"], "Error", "error");
        }else{
          // The restaurant exists
          this.resenaService.resenas = res as Resena[];
        }
      });
    }
  }


  // vars for angular FormsModule, used because or error https://stackoverflow.com/questions/54365670/angular-7-build-prod-error-property-value-does-not-exist-on-type-component
  // it appears that no all var forms has this problem, so maybe its a bug
  contrasenaUser: any;

}