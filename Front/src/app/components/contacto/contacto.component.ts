import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

import { EmailService } from 'src/app/services/email.service';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent{
  constructor(public email: EmailService) { }

  contactForm(form) {
    this.email.sendEmailContact(form).subscribe((res) => {
        Swal.fire("Formulario de contacto", 'Correo enviado satisfactoriamente.\n' +
        'Pronto de contactaremos', "success");
    });
  }





  // minimap titles: validators
  form = new FormGroup({
    nombre: new FormControl('', Validators.required),
    nombreRestaurante: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.pattern(/[^@]+@[^\.]+\..+/)]),
    telefonoContacto: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{7,10}/)]),
    mensaje: new FormControl('', Validators.required)
  });

}
