// Componentes y modulos para trabajar con el routing
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

// Componentes en el routing
import { HomeComponent } from './components/home/home.component';
import { MapaComponent } from './components/home/mapa/mapa.component';
import { RestauranteComponent } from './components/restaurante/restaurante.component';
import { HistoriaComponent } from './components/home/historia/historia.component';
import { MejorPlatoComponent } from './components/home/mejor-plato/mejor-plato.component';
import { MejorPromoComponent } from './components/home/mejor-promo/mejor-promo.component';
import { RestauranteListaComponent } from './components/home/restaurante-lista/restaurante-lista.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { ViewAdminComponent } from './components/view-admin/view-admin.component';
import { LoginComponent } from './components/login/login.component';
import { UserAdminProfileComponent } from './components/login/user-admin-profile/user-admin-profile.component';
import { RestaurantProfileComponent } from './components/login/restaurant-profile/restaurant-profile.component';

// Guards
import { LoginGuardGuard } from './guards/login-guard.guard';

// Rev used to work with fragment insted onf i="#sectionpage"
// [routerLink]='"."' [fragment]="aFlotanteHistorias" 
const routerOptions: ExtraOptions = {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    // onSameUrlNavigation: 'ignore',
    // scrollOffset: [0, 64]
  };

const appRoutes: Routes = [
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},

    {path: 'mapa', component: MapaComponent},
    {path: 'historias', component: HistoriaComponent},
    {path: 'platos', component: MejorPlatoComponent},
    {path: 'promociones', component: MejorPromoComponent},
    {path: 'restaurantesLista', component: RestauranteListaComponent},
    
    {path: 'contacto', component: ContactoComponent},
    {path: 'login', component: LoginComponent},
    // Only user can see the profile view
    {path: 'view-admin', component: ViewAdminComponent, canActivate: [LoginGuardGuard]},
    {path: 'view-profile', component: UserAdminProfileComponent, canActivate: [LoginGuardGuard]},
    {path: 'view-profile-restaurant', component: RestaurantProfileComponent, canActivate: [LoginGuardGuard]},
    // Ruta para el marcador de cada restaurante en el mapa
    {path: 'restaurante/:id', component: RestauranteComponent},
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, routerOptions);
