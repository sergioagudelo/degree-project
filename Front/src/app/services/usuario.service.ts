import { Injectable } from '@angular/core';
// Http is the way to connect with our server
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// Packages used to methos login and register
// Rev read about tap vs describe, observable, behaviorSujext and rxjs
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

import Swal from 'sweetalert2'

import { FormBuilder, FormGroup } from '@angular/forms';


// modelos
import { Usuario } from '../models/usuario';
import { dataUserJWT } from '../models/jwt-response-user';
import { AuthService } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  selectedUsuario: Usuario;
  usuarios: Usuario[];

  // Consulting URL on backend for users
  readonly URL_API = environment.urlBack + '/api/usuarios';

  // Token used to manage logged and unlogged users

  constructor(
    public http: HttpClient,
    public authService: AuthService
    ) {
    this.selectedUsuario = new Usuario();
  }

  postUsuario(usuario: Usuario, file?: File) {
    // const fdFile = new FormData();
    // if(file){
    //   fdFile.append('image', file);
    // }
    // for ( var key in usuario ) {
    //   fdFile.append(key, usuario[key]);
    // }
    return this.http.post(this.URL_API, usuario);
    // return this.http.post(this.URL_API, { 'usuario': usuario, 'image': fdFile });
  }

  getUsuarios() {
    return this.http.get(this.URL_API);
  }

  putUsuario(usuario, file?: File, accessToken?: string) {
    const fdFile = new FormData();
    if(file){
      fdFile.append('image', file);
    }
    for ( var key in usuario ) {
      // Way to send JSON and file by http
      fdFile.append(key, usuario[key]);
    }
    if(accessToken){
      fdFile.append('accessToken', accessToken);
    }
    return this.http.put(this.URL_API + `/${usuario._id}`, fdFile );
  }

  deleteUsuario(_id: String) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }

  // Returns a filtered array used to show an specific user
  getFilteredUsers(data, inputSelected){
    return this.http.get(this.URL_API + `/getFilteredUsers` + `/${data}` + `/inputSelected` + `/${inputSelected}`);
  }

  // Update an sepecific attribute of user
  updateCampo(userId, dataUpdated, value){
    return this.http.put(this.URL_API + `/updateCampo` + `/${userId}`+ `/dataUpdated` + `/${dataUpdated}`, {value});
  }

  // Returns a user by id
  getUser(id: String){
    return this.http.get(this.URL_API + `/getUser` + `/${id}`);
  }

  // Returns a user by accessToken
  getUserByAccessToken(accessToken: String){
    return this.http.get(this.URL_API + `/getUserByAccessToken` + `/${accessToken}`);
  }

  // Returns a user by accessToken
  getUserByEmailAndUpdatePassword(email: String, newPassword: String){
    return this.http.get(this.URL_API + `/getUserByEmailAndUpdatePassword` + `/${email}` + `/newPassword` + `/${newPassword}`);
  }

  //Method used to register an user by a secure way
  register(user: Usuario, file?: File) {
    const fdFile = new FormData();
    if(file){
      fdFile.append('image', file);
    }
    for ( var key in user ) {
      fdFile.append(key, user[key]);
    }
    return this.http.post(this.URL_API + `/register`, fdFile);
      // .pipe(tap(
      //   (res: dataUserJWT) => {
      //     if(res['status'] == 200){
      //       if (res) {
      //         // guardar token
      //         this.saveToken(res.accessToken, res["usuario"].nombre);
      //       }
      //     }else{

      //     }
      //   })
      // );
  }

  //Method used to login an user by a secure way
  login(user: Usuario) {
    return this.http.post(this.URL_API + `/login`, user);
      // .pipe(tap(
      //   async (res: dataUserJWT) => {
      //     if (res['status'] == 200) {
      //       if(res['usuario'].bloqueado == true ){
      //         Swal.fire({
      //           title: 'Oops...', 
      //           html: 'Su usuario ha sido bloqueado.',
      //           type: 'error'
      //         });
      //       }else{
      //         // Save token on localStorage
      //         this.saveToken(res.accessToken, res['usuario'].nombre);
      //       }
      //     }
      //   })
      // );
  }

  //Method used to logout an user by a secure way, clean token and accessToken
  // Rev fix method, work just as worked with restaurants
  logout() {
    let accessToken = localStorage.getItem("ACCESS_TOKEN") || null;
    // Rev if I didn´t use .subscribe the function will not work
    // Rev fiex method instead of parameter to send accessToken use body
    this.deleteToken("ACCESS_TOKEN", "USER_NAME");
    return this.http.put(this.URL_API + `/logout` + `/${accessToken}`, accessToken);
    
    
    // localStorage.removeItem("EXPIRES_IN");
    // localStorage.removeItem("CURRENT_USER");

    // Rev Used on https://www.youtube.com/watch?v=gSy4vY_x2-w to logout on server too
    // const url_api = `http://localhost:3000/api/Users/logout?access_token=${accessToken}`;
    // return this.htttp.post<UserInterface>(url_api, { headers: this.headers });

  }

  //Method used to sets user token on localStorage
  saveToken(token: string, user: string): void {
    localStorage.setItem("ACCESS_TOKEN", token);
    localStorage.setItem("USER_NAME", user);
  }

  //Method used to sets user token on localStorage
  deleteToken(token: string, user: string): void {
    localStorage.removeItem(token);
    localStorage.removeItem(user);
  }

  // Method used to set the actual user on localstorage
  // public setUser(user): void {
  //   let user_string = JSON.stringify(user);
  //   localStorage.setItem("CURRENT_USER", user_string);
  // }

  // Method used to check user by guards (canActivate) on routes
  // Rev fix method, work with accessToken in database, findone by accessToken and username, if exists then he can enter de route, otherwise not
  // getCurrentUser() {
  //   let user_string = localStorage.getItem("USER_NAME");
  //   if (!isNullOrUndefined(user_string)) {
  //     return true
  //   } else {
  //     return null;
  //   }
  // }

  // Return the token o localStorage
  getToken(): string {
    return localStorage.getItem("ACCESS_TOKEN");
  }

  // changes password
  changePassword(userId: string, currentPassword: string, newPassword: string){
    return this.http.put(this.URL_API + `/changePassword` + `/${userId}`+ `/currentPassword` + `/${currentPassword}`, {newPassword});
  }

  // Minimap titles: login with redes
  // Login or Register an user by redes
  loginWithRedes(user, userName){
    return this.http.post<dataUserJWT>(this.URL_API + `/loginWithRedes/` + userName,
      user).pipe(tap(
        (res: dataUserJWT) => {
          if (res) {
            if(res["usuario"].bloqueado == true ){
              alert('Su usuario ha sido bloqueado.');
              this.authService.signOut();
            }else{
              // Save token on localStorage
              this.saveToken(res.accessToken, res["usuario"].nombre);
            }
          }
        })
      );
  }
}