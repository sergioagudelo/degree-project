import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Restaurante } from '../models/restaurante';
import { environment } from 'src/environments/environment';
import { UserAdminProfileComponent } from '../components/login/user-admin-profile/user-admin-profile.component';
import { ProtractorExpectedConditions } from 'protractor';

@Injectable({
  providedIn: 'root'
})

export class RestauranteService {

  selectedRestaurante: Restaurante;
  plato: Restaurante;
  restaurantes: Restaurante[];
  restaurante: any;

  readonly URL_API = environment.urlBack + '/api/restaurantes';

  constructor(public http: HttpClient) {
    this.selectedRestaurante = new Restaurante();
  }

  



  // Minimap titles CRUD Restaurants
  // Create a new restaurant
  postRestaurante(restaurante: Restaurante) {
    return this.http.post(this.URL_API, restaurante);
  }

  // Returns a json with all the restaurants
  getRestaurantes() {
    return this.http.get(this.URL_API);
  }

  // Calls a function that Update a restaurante
  putRestaurante(restaurante: Restaurante, accessToken?: string) {
    if(accessToken){
      restaurante.accessToken = accessToken;
    }
    return this.http.put(this.URL_API + `/editRestaurante` + `/${restaurante._id}`, restaurante);
  }

  // Calls a function that Delete a restaurant
  deleteRestaurante(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }

  // Calls a function that Gets an specific restaurant through an id
  getRestaurante(_id) {
    return this.http.get(this.URL_API + `/getRestaurante` + `/${_id}`);
  }

  // Calls restauranteInf for maps
  getRestauranteMapa() {
    return this.http.get(this.URL_API + `/restauranteInf/1`);
  }

  // Calls restauranteInf for historia
  getRestauranteHistoria() {
    return this.http.get(this.URL_API + `/restauranteInf/2`);
  }

  // Calls restauranteInf for platos
  getRestaurantePlatos() {
    return this.http.get(this.URL_API + `/restauranteInf/3`);
  }

  // Calls restauranteInf2
  // getCategoriaPlato(categoria, _id) {
  //   return this.http.get(this.URL_API + `/4` + `/${categoria}` + `/${_id}`);
  // }

  // Calls restauranteInf2
  // getDiaPromo(dia, _id) {
  //   return this.http.get(this.URL_API + `/5` + `/${dia}` + `/${_id}`);
  // }

  // Returns a filtered array used to show an specific restaurant
  getFilteredRestaurants(nombre){
    return this.http.get(this.URL_API + `/getFilteredRestaurants` + `/${nombre}`);
  }

  // Update an sepecific attribute of Restaurant
  updateCampo(restId, dataUpdated, value:Boolean){
    return this.http.put(this.URL_API + `/updateCampo` + `/${restId}` + `/dataUpdated` + `/${dataUpdated}`, {value});
  }





  // Minimap titles: methos for view-admin
  // Returns all (blocked and non-blocked) restaurants
  getAllRestaurants() {
    return this.http.get(this.URL_API + `/getAllRestaurants`);
  }

  // Calls a function that Update a restaurante from view admin
  putRestauranteFromViewAdmin(restaurante: Restaurante, accessToken?: string) {
    if(accessToken){
      restaurante.accessToken = accessToken;
    }
    return this.http.put(this.URL_API + `/editRestauranteFromViewAdmin` + `/${restaurante._id}`, restaurante);
  }




  // minimap titles methods for profile restaurant
  // creates local image
  changePhotoLocal(id: string, fileLocal){
    const fdFile = new FormData();
    if(fileLocal){
      fdFile.append('local', fileLocal);
    }
    return this.http.put(this.URL_API + `/changePhotoLocal`+ `/${id}`, fdFile);
  }

  // Creates logo image
  changePhotoLogo(id: string, fileLogo){
    const fdFile = new FormData();
    if(fileLogo){
      fdFile.append('logo', fileLogo);
    }
    return this.http.put(this.URL_API + `/changePhotoLogo`+ `/${id}`, fdFile);
  }

  // Creates a palte for an specific restaurant
  createPlate(idRestaurant: string, plato, file, restaurantCategories){
    const fd = new FormData();
    fd.append('image', file);
    for ( var key in plato ) {
      if(plato[key] != null){
        // converts a JSON in a FormData to send an image by body
        fd.append(key, plato[key]);
      }
    }
    fd.append("restaurantCategories", restaurantCategories);
    return this.http.put(this.URL_API + `/createPlate`+ `/${idRestaurant}`, fd);
  }

  // Creates a palte for an specific restaurant
  deletePlate(idRestaurant: string, foto: string){
    // Its the onlye way to send body by delete ProtractorExpectedConditions, it was necesary because of the route of photos by URL get err
    const photoPlate = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: {
        photoPlate: foto,
      }
    }
    return this.http.delete(this.URL_API + `/deletePlate/${idRestaurant}`, photoPlate);
  }





  // Minimaps titles: methods for Home
  // Returns a json with all the restaurants and califications
  getRestaurantesWithCalifications() {
    return this.http.get(this.URL_API + `/getRestaurantesWithCalifications`);
  }
  
  // filters
  // Returns all the diferent categorias
  getDifferentCategorias(){
    return this.http.get(this.URL_API + `/getDifferentCategorias`);
  }

  getFilteredSearch(selectedCategoria, selectedPrecio, restaurantsAboveCalification){
    return this.http.get(this.URL_API + `/getFilteredSearch/selectedCategoria` + `/${selectedCategoria}` + `/selectedPrecio` + `/${selectedPrecio}`
    + `/restaurantsAboveCalification` + `/${restaurantsAboveCalification}`);
  }








  // Minimap titles: login
  // Method used to login an user by a secure way
  // Rev This method works similarly as login user
  loginRestaurante(restaurante: Restaurante){
    return this.http.post(this.URL_API + `/loginRestaurante`, restaurante);
  }

  //Method used to logout an user by a secure way, clean token and accessToken
  logoutRestaurante() {
    let accessToken = localStorage.getItem("ACCESS_TOKEN");
    return this.http.put(this.URL_API + `/logoutRestaurante`, { accessToken: accessToken });
  }

  // Returns a rest by accessToken
  getRestaurantByToken(accessToken: String){
    return this.http.get(this.URL_API + `/getRestaurantByToken` + `/${accessToken}`);
  }
  // Method used to delete restaurant token on localStorage
  deleteTokenRestaurant(): void {
    localStorage.removeItem("ACCESS_TOKEN");
    localStorage.removeItem("USER_NAME");
    localStorage.removeItem("FLAG");
  }




  // Minimap titles: view profile restaurant
  // changes password
  changePassword(accessToken:string, Id: string, currentPassword: string, newPassword: string){
    // Rev we can send current password on the body
    const userInfo = {
      accessToken: accessToken,
      currentPassword: currentPassword,
      newPassword: newPassword
    }
    return this.http.put(this.URL_API + `/changePassword` + `/${Id}`+ `/currentPassword`, {userInfo});
  }
}
