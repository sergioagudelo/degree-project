import { TestBed, inject } from '@angular/core/testing';

import { ResenaService } from './resena.service';

describe('ResenaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResenaService]
    });
  });

  it('should be created', inject([ResenaService], (service: ResenaService) => {
    expect(service).toBeTruthy();
  }));
});
