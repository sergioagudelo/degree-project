import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  readonly URL_API = environment.urlBack + '/api/contacto';

  constructor(public http: HttpClient) { }

  // Sends an email to book bookeat2019@gmail.com form a restaurant
  // email sended form contacto component
  sendEmailContact(body) {
    return this.http.post(this.URL_API + `/contacto`, body);
  }

  // Sends an email from bookeat2019@gmail.com to registered user or restaurant
  // emaild sended form contacto component
  sendEmailRecoverPassword(email, nombre, newPassword: String) {
    let usuario = { 
      email: email,
      nombre: nombre,
      newPassword: newPassword
    }
    return this.http.post(this.URL_API + `/sendEmailRecoverPassword`, usuario);
  }
}