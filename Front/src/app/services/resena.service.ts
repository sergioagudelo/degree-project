import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Resena } from '../models/resena';
import { environment } from 'src/environments/environment';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class ResenaService {

  selectedResena: Resena;
  resenas: Resena[];

  readonly URL_API = environment.urlBack + '/api/resenas';

  constructor(private http: HttpClient) {
    this.selectedResena = new Resena();
  }

  postResena(resena: Resena) {
    return this.http.post(this.URL_API, resena);
  }

  getResenas() {
    return this.http.get(this.URL_API);
  }

  putResena(resena: Resena) {
    return this.http.put(this.URL_API + `/${resena._id}`, resena);
  }

  deleteResena(idResena: string) {
    return this.http.delete(this.URL_API + `/delete` + `/${idResena}`);
  }

  getResena(_id) {
    return this.http.get(this.URL_API + `/getResenas` + `/${_id}`);
  }





  // Minimap titles: califications
  // Returns the califications average for a restaurant
  getCalificacionRestaurante(_id) {
    return this.http.get(this.URL_API + `/getCalificacionRestaurante`+ `/${_id}`);
  }

  // minimaps titles: methos used to filter Home
  // Returns an array of reestaurants based on califictions
  getRestaurantsAboveCalification(selectedCalificacion: number) {
    return this.http.get(this.URL_API + `/getRestaurantsAboveCalification`+ `/${selectedCalificacion}`);
  }





  // Minimap titles: view admin
  // Returns a filtered array of resenas
  getFilteredResenas(usuario){
    return this.http.get(this.URL_API + `/getFilteredResenas` + `/${usuario}`);
  }
}
