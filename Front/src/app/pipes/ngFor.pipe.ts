import {Pipe, PipeTransform} from '@angular/core';

//pipe utilizada apra recorrer un array de 3 en 3 elementos

@Pipe({name: 'demoNumber'})
export class DemoNumber implements PipeTransform {
    transform(value, args:string[]) : any {
    let res = [];
    for (let i = 0; i < value; i = i + 3) {
      res.push(i);
    }
     return res;
    }
}
