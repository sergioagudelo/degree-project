const Resena = require('../models/resena');
const resenaCtrl = {};

resenaCtrl.getResenas = async (req, res, next) => {
    const resenas = await Resena.find();
    res.json(resenas);
};

resenaCtrl.createResena = async (req, res, next) => {
    const resena = new Resena({
        restaurante: req.body.restaurante,
        usuario: req.body.usuario,
        texto: req.body.texto,
        calificacion: req.body.calificacion
    });
    await resena.save();
    res.json({ status: 'Resena created' });
};

resenaCtrl.getResena = async (req, res, next) => {
    const { id } = req.params;
    const resenas = await Resena.find({ restaurante: id });
    res.json(resenas);
};

resenaCtrl.editResena = async (req, res, next) => {
    const { id } = req.params;
    const resena = {
        restaurante: req.body.restaurante,
        usuario: req.body.usuario,
        texto: req.body.texto,
        calificacion: req.body.calificacion
    };
    await Resena.findByIdAndUpdate(id, { $set: resena }, { new: true });
    res.json({ status: 'Resena Updated' });
};

resenaCtrl.deleteResena = async (req, res, next) => {
  console.log(req)
    let resenha = await Resena.findByIdAndRemove(req.params.id);
    console.log(resenha)
    if(resenha){
        res.json({ status: 200, message: 'Reseña eliminada' });
    }else{
        res.json({status: 400, message: 'Ha ocurrido un error, vuelva a intentarlo mas tarde.'});
    }
};





// Minimap titles: methods for view restaurant
// Returns the average califications for a restaurant
resenaCtrl.getCalificacionRestaurante = async (req, res, next) => {
    const { id } = req.params;

    const promedio = await Resena.aggregate([
        {$match: { restaurante: id }},
        {
            $group: {
            _id: null,
            promCalification: { $avg: "$calificacion" }
            }
        }
    ]);

    res.json(Math.trunc(promedio[0]['promCalification']));
};





// Minimap titles: methods for home califications
// Returns the average califications for a restaurant
resenaCtrl.getCalificacionRestaurante = async (req, res, next) => {
    const { id } = req.params;

    const promedio = await Resena.aggregate([
        {$match: { restaurante: id }},
        {
            $group: {
            _id: null,
            promCalification: { $avg: "$calificacion" }
            }
        }
    ]);
    console.log()

    res.json(Math.trunc(promedio[0]['promCalification']));
};






// minimaps titles: methos used to filter Home
// Returns an array of id reestaurants when restaurant calification is greater than selectedCalificacion
// Rev this method can by rebuild and improved fixing resenas model or using beter mongoose like this (https://stackoverflow.com/questions/26810599/calculating-average-in-mongoose)
resenaCtrl.getRestaurantsAboveCalification = async (req, res, next) => {
    const { selectedCalificacion } = req.params;

    // Rev its not neccesary return promCalification and truncPromCalification, we only need the restaruants with califications above selectedCalificacion
    // worked like this jus to practice a litle bit
    const restaurantesAboveCalification = await Resena.aggregate([
        {
          $group: {
            _id: '$restaurante',
            promCalification: {
              $avg: '$calificacion'
            }
          }
        }, {
          $addFields: {
            truncPromCalification: {
              $trunc: [
                '$promCalification'
              ]
            }
          }
        }, {
          $match: {
            truncPromCalification: {
              $gte: Number(selectedCalificacion)
            }
          }
        }, {
          $sort: {
            truncPromCalification: -1
          }
        }, {
          $project: {
            _id: 1
          }
        }
      ]);

    res.json(restaurantesAboveCalification)
};





// Minimap titles: view admin
// Returns an array of filtered resenas by user
resenaCtrl.getFilteredResenas = async (req, res, next) => {
    if(req.params.usuario != null){
        const resenas = await Resena.find({'usuario': {$regex: ".*"+ req.params.usuario + ".*", $options:"i"}});
        if(resenas.length > 0){
            res.json(resenas);
        }else{
            res.json({ status: 'El usuario ' + req.params.usuario + ' no existe.' });
        }
    }
};


 

module.exports = resenaCtrl;