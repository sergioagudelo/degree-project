// Used to work with nodemailer to send emails
const configMensaje = require('../libs/email/configMensaje');
const resenaCtrl = {};

// email sended from contacto component
resenaCtrl.sendEmailContacto = async (req, res, next) => {
    configMensaje(req.body, 'contact');
    res.status(200).send();
};

// email sended from contacto recovery password
resenaCtrl.sendEmailRecoverPassword = async (req, res, next) => {
    configMensaje(req.body, 'resetPassword');
    res.status(200).send();
};


module.exports = resenaCtrl;