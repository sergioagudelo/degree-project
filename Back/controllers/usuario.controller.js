const Usuario = require('../models/usuario');
const path = require('path');
// Fs (file system) work with files
// const fs = require('fs-extra');
const usuarioCtrl = {};
const noProfilePhoto = "assets/no-profile-photo.png"
// Rev Dev if I want best responses from my server I should use something like try catch, this can make easier
// the way to check unique values, for example when a user register on owr app
usuarioCtrl.createUsuario = async (req, res, next) => {
    // let file;
    // if(req.file){
    //     file = req.file.path;
    // }else{
    //     file = "uploads\\no-profile-photo.png";
    // }
    const user = req.body;

    const usuario = new Usuario({
        fotoPerfil: noProfilePhoto,
        usuario: user.usuario,
        nombre: user.nombreUsuario,
        correo: user.correo,
        fechaNacimiento: user.fechaNacimiento,
        celular: user.celular,
        contrasena: bcrypt.hashSync(user.contrasenaUser, salt)
    });
    await usuario.save();
    res.json({ status: 'Usuario created' });
};

usuarioCtrl.getUsuarios = async (req, res, next) => {
    const usuarios = await Usuario.find();
    res.json(usuarios);
};

usuarioCtrl.editUsuario = async (req, res, next) => {
    const user = req.body;

    if(user.accessToken != 'null'){
        // checks if accessToken its valid
        // Rev it should be better if we check that the user X has the accessToken Y
        jwt.verify(user.accessToken, SECRET_KEY, async (err,decoded) => {
            if(!err){
                // if accessToken is valid, then work as usual
                const { id } = req.params;
    
                const currentUser = await Usuario.findOne({'_id': id}, {contrasena: 1, fotoPerfil: 1, provider: 1, _id: 0});

                if(req.file){
                    uploadFileAws(route, req.file);
                    deleteFile(route, currentUser.fotoPerfil);
                }else {
                    image = currentUser.fotoPerfil;
                }

                if(currentUser){
                    // OLD METHOD
                    // used for multer
                    // if(req.file){
                    //     // We make sure that we didn´t delete de noProfilePhoto
                    //     if(user.fotoPerfil != noProfilePhoto){
                    //         // await fs.unlink(path.resolve(currentUser.fotoPerfil));
                    //     }
                    //     file = req.file.path;
                    // }else{
                    //     file = currentUser.fotoPerfil;
                    // }
        
                    // Checks if both password are the same
                    // Rev if passwords are different then will work too, fix this
                    // Used like this because if user want or not to change password
                    const resultPassword = user.contrasenaUser == currentUser.contrasena;
                    if(resultPassword){
                        // If method doesn´t update password
                        var usuario = {
                            fotoPerfil: image,
                            usuario: user.usuario,
                            nombre: user.nombreUsuario,
                            correo: user.correo,
                            fechaNacimiento: user.fechaNacimiento,
                            celular: user.celular,
                            contrasena: user.contrasenaUser,
                            provider: currentUser.provider
                        };
                    }else{
                        // If method update password
                        var usuario = {
                            fotoPerfil: image,
                            usuario: user.usuario,
                            nombre: user.nombreUsuario,
                            correo: user.correo,
                            fechaNacimiento: user.fechaNacimiento,
                            celular: user.celular,
                            contrasena: bcrypt.hashSync(user.contrasenaUser, salt),
                            provider: currentUser.provider
                        };
                    }
                    
                    await Usuario.findByIdAndUpdate(id, { $set: usuario });
                    res.json({ status: 200, message: 'Usuario Actualizado.' });
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }
            else{
                // if accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};

usuarioCtrl.deleteUsuario = async (req, res, next) => {
    let usuario = await Usuario.findByIdAndRemove(req.params.id);
    if(usuario.fotoPerfil != noProfilePhoto){
        // OLD METHOD
        // We make sure to no delete uploads\\no-profile-photo.png
        // await fs.unlink(path.resolve(usuario.fotoPerfil));
        deleteFile(route, usuario.fotoPerfil);
    }
    res.json({ status: 'Usuario Deleted' });
};

// Returns a filter array of users by user, nombre or email to view-admin
usuarioCtrl.getFilteredUsers = async (req, res, next) => {
    let query = "";
    let data = "";
    if(req.params.dataSearch != null){
        // Rev repeated code, define something like query = 'usuario':  '/.*' + req.params.dataSearch + '.*/' inside if and then query outsife if
        if(req.params.inputSelected == 1){
            // If selected input user
            data = "user";
            query = await Usuario.find({'usuario': {$regex: ".*"+ req.params.dataSearch + ".*", $options:"i"}}); 
        }else if(req.params.inputSelected == 2){
            // If selected input name
            data = "nombre";
            query = await Usuario.find({'nombre':  {$regex: ".*"+ req.params.dataSearch + ".*", $options:"i"}}); 
        }else if(req.params.inputSelected == 3){
            // If selected input email
            data = "email";
            query = await Usuario.find({'correo': {$regex: ".*"+ req.params.dataSearch + ".*", $options:"i"}});
        }
        console.log(query)
        if(query.length > 0){
            res.json(query);
        }else{
            res.json({ status: 'El usuario con ' + data + ' ' + req.params.dataSearch + ' no existe.'});
        }
    } 
};

// Update an sepecific attribute of user
// Rev method, I know its working for bloqueado, but not sure about accessToken
usuarioCtrl.updateCampo = async (req, res, next) => {
    const { id } = req.params;
    // with this parameter we can make this mehtod universal, I need a way to identify wich one parameter the method will update
    const { dataUpdated } = req.params;
    const { value } = req.body;
    let update = {};

    // which attr will be update
    if(dataUpdated == 'bloqueado'){
        update = {
            'bloqueado': value
        }
    }else if(dataUpdated == 'accessToken'){
        update = {
            'accessToken': value
        }
    }

    await Usuario.findOneAndUpdate({ "_id" : id }, update );
    // await Usuario.findOneAndUpdate({ "_id" : id }, { "bloqueado" : value });
    res.json({ status: 'Se actualizo el campo ' + dataUpdated + ' a: ' + value +
    ' del usuario con id: ' + id + '.'});
};

// Returns a specific user by id
usuarioCtrl.getUser = async (req, res, next) => {
    const { id } = req.params;
    let usuario = await Usuario.findOne({'_id': id});
    res.json(usuario);
};





// Minimap titles: Login and register
// Packages used to encrypt our password
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// aleatory fragment used to generate the hash next to the password
const salt = 12;
//  Secret key used to generate our encrypted password
const SECRET_KEY = 'secretkey123456';
const expiresIn = 24 * 60 * 60; // 86400 Seconds

//Method used to register an user by a secure way
usuarioCtrl.registerUser = async (req, res, next) => {
    if(req.file){
        uploadFileAws(route, req.file)
    }
    const user = req.body;

    // The ideal way is to not use our database _id, its better if we used another id
    // de esta forma no ponemos en riesgo la integridad de nuestra BD
    let date = new Date();
    date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
    const accessTokenName = jwt.sign({ tokenData: user.nombre + date },
        SECRET_KEY, {
            expiresIn: expiresIn
        });

    const usuario = new Usuario({
        fotoPerfil: image,
        usuario: user.usuario,
        nombre: user.nombre,
        correo: user.correo,
        fechaNacimiento: user.fechaNacimiento,
        celular: user.celular,
        contrasena: bcrypt.hashSync(user.contrasena, salt),
        bloqueado: false,
        accessToken: accessTokenName
    });

    await usuario.save(async (err, usuario) => {
        // Error by unique data
        if (err && err.code === 11000) return res.json({status: 409, message: 'El usuario y/o email ya esta en uso.'});
        else if (err) return res.json({status: 500, message: 'Server error!'});
        // response
        if(usuario){
            // response 
            await Usuario.findOneAndUpdate({ _id: usuario._id}, { accessToken: accessTokenName });
            res.json( { status: 200, usuario: usuario, accessToken: usuario.accessToken });
        }else{
            res.json({status: 409, message: 'No se pudo crear el usuario.'});
        }
    });
};

//Method used to login an user by a secure way
usuarioCtrl.loginUser = async (req, res, next) => {
    const userData = {
      dataLogin: req.body.email,
      password: req.body.contrasena
    }

    await Usuario.findOne({ correo: userData.dataLogin }, async (err, usuario) => {
      if (err) return res.json({status: 500, message: 'Server error!'});
  
      if (!usuario) {
        // Email does not exist
        // By security we didn´t tell that the username or email dosen´t exists
        res.json({status: 409, message: 'Usuario incorrecto.'});
      } else {
        const resultPassword = bcrypt.compareSync(userData.password, usuario.contrasena);
        if (resultPassword) {

            // The ideal way is to not use our database _id, its better if we used another id
            // de esta forma no ponemos en riesgo la integridad de nuestra BD
            let date = new Date();
            date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
            const accessTokenName = jwt.sign({ tokenData: usuario.nombre + date },
                SECRET_KEY, {
                    expiresIn: expiresIn
                }); 

            // Isn´t neccesary ckech if user exist at this point because its already checked above
            await Usuario.findOneAndUpdate({ _id: usuario._id}, { accessToken: accessTokenName });
            // response 
            res.json({status: 200, usuario: usuario, accessToken: accessTokenName});
        } else {
          // password wrong
          // By security we didn´t tell that passwords dosen´t match
          res.json({status: 409, message: 'Clave incorrecta.'});
        }
      }
    });
};

// Changes password
usuarioCtrl.changePassword = async (req, res, next) => {
    await Usuario.findById( req.params.id, async (err, usuario) => {        
        if (!usuario) {
            // User doesn´t exists
            // By security we didn´t tell that the username or email dosen´t exists
            res.json({ status:409, message: 'Usuario incorrecto.'});
        }else{
            // User exists
            const resultPassword = bcrypt.compareSync(req.params.currentPassword, usuario.contrasena);            
            if(resultPassword){
                await Usuario.findOneAndUpdate({ _id: req.params.id }, { contrasena: bcrypt.hashSync(req.body.newPassword, salt) });
                res.json({ status:200, message: "La contraseña del usuario  " + usuario.nombre + " ha sido cambiada exitosamente." });
            }else{
                res.json({ status: 409, message: 'Contraseña incorrecta.'});
            }
        }
    });
};

//Method used to logout an user by a secure way and delete accestoken
usuarioCtrl.logoutUser = (req, res, next) => {
    // Checks if accessToken exists
    if(req.params.accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(req.params.accessToken, SECRET_KEY, async (err,decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                let usuario = await Usuario.findOneAndUpdate({ accessToken: req.params.accessToken}, { accessToken: null });
                if(usuario){
                    res.json({status: 200, message: "Usuario con accessToken " + req.params.accessToken + " ha sido deslogueado."});
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No has enviado un accessToken'});
    }
};

// Returns a specific user by id and update his password
usuarioCtrl.getUserByEmailAndUpdatePassword = async (req, res, next) => {
    const { email } = req.params;
    const { newPassword } = req.params;
    console.log(newPassword)

    await Usuario.findOneAndUpdate({'correo': email}, {contrasena: bcrypt.hashSync(newPassword, salt)}, async (err, usuario) => {
        console.log(usuario)
        if (err) return res.json({status: 500, message: 'Server error!'});
    
        if (!usuario) {
            // Email does not exist
            res.json({status: 409, message: 'El correo ingresado no se encuentra registrado en BookEat.'});
        } else {
            // Email exists
            res.json({status: 200, message: 'Hemos enviado un correo a tu E-mail ' + email + 
            ' donde podras continuar con el proceso de restauración de contraseña.',
            usuario: {nombre: usuario.nombre}});
        }
    });
};

// Returns a specific user by accessToken
usuarioCtrl.getUserByAccessToken = async (req, res, next) => {
    const { accessToken } = req.params;
    // Checks if accessToken existss
    if(accessToken != 'null'){
        jwt.verify(req.params.accessToken, SECRET_KEY, async (err, decoded) => {
            // Checks if accessToken its valids
            if(err){
                res.json({status: 401, message: 'Desautorizado'});
            }else{
                // If accessToken is valid, then work as usuals
                const usuario = await Usuario.findOne({'accessToken': accessToken});
                if(usuario){
                    res.json({status: 200, usuario: usuario});
                }else{
                    res.json({status: 400, message: 'El usuario no existe.'});
                }
            }
        });
    }else{
        // If accessToken isn´t valid, unahutorized the users
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};

// Minimap titles: login with redes
// Login or Register an user by redes
usuarioCtrl.loginWithRedes = async (req, res, next) => {
    let date = new Date();
    date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
    const accessTokenName = jwt.sign({ tokenData: req.body.name + date },
        SECRET_KEY, {
            expiresIn: expiresIn
        });

    const userData = new Usuario({
        fotoPerfil: noProfilePhoto,
        accessToken: accessTokenName,
        usuario: req.params.userName,
        nombre: req.body.name,
        correo: req.body.email,
        provider: req.body.provider
    });

    await Usuario.findOne({ correo: userData.correo }, async (err, usuario) => {
      if (err) return res.json({status: 500, message: 'Server error!'});
  
      if (!usuario) {
        // Email does not exist, the method register a new user
        await userData.save( err => {
            // Error by unique data
            if (err) return res.json({status: 500, message: 'Server error!'});
            else{
                res.json( {status: 200, usuario: userData, accessToken: accessTokenName});
            }
        });
      }else {
        // User exists, the method login a existent user
        await Usuario.findOneAndUpdate({ _id: usuario._id}, { accessToken: accessTokenName }, (err, usuario) => {
            if (err) return res.json({status: 500, message: 'Server error3!'});
            if(usuario){
                // response
                res.json( {status: 200, usuario: usuario, accessToken: accessTokenName});
            }else{
                res.json({status: 409, message: 'No se pudo crear el usuario.'});
            }
        });
      }
    });
};





// minimap titles: upload files to aws buckets
// Used for aws upload images on buckets s3
var AWS = require('aws-sdk')
var s3 = new AWS.S3({
    accessKeyId: global.gConfig.accessKeyId,
    secretAccessKey: global.gConfig.secretAccessKey
});
// route bucket
let route = '/usuarios';
// name image wich will be send to DB
var image = "assets/no-profile-photo.png";
// Upload files to aws s3 bucket
async function uploadFileAws(route, file){
    // Params used to deploy images on bucket s3 aws
    const paramsUpload = {
        Bucket: global.gConfig.bucket + route,
        Key: Date.now().toString() + path.extname(file.originalname), // File name you want to save as in S3
        Body: file.buffer,
        ACL: 'public-read',
        ContentType: 'image'
    };

    // Rev fix file assignation, if file cannot be upload then we will have a problem
    // its neccesary to wait until file its upload to get a real url a not a "url quemada"
    // best way its to put update query inside s3.upload, not done because s3.upplaod tackes so much time and the update looks slow
    image = paramsUpload.Key;

    // Uploading files to the bucket
    await s3.upload(paramsUpload, async (err, data) => {
        if (err) {
            console.log(err);
        }
        console.log(`File uploaded successfully. ${data.Location}`);
        
        // Rev file assign should work like this, but upload the file take so much time
        // file = data.Location;
    });
}

// delete files from aws s3 bucket
let deleteFile = async function deleteFileAws(route, oldFile){
    var paramsDelete = {  
        Bucket: global.gConfig.bucket + route,
        Key: oldFile
    };

    s3.deleteObject(paramsDelete, function(err, data) {
        if (err) console.log(err, err.stack);
        else     console.log('Deleted succesfully');
    });
}





module.exports = usuarioCtrl;