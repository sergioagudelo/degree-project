// We used controllers because its easier to structure an app when it grows.
// Controllers interact between routes and data model.
// Controllers define the methods used on our Routes to work with the DB.
// Rev fix variable Restaurante to RestauranteModel (its an object).
const Restaurante = require('../models/restaurante');
// By this way we can add functions to our object restauranteCtrl
const restauranteCtrl = {};

// Packages used to encrypt our password
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
// aleatory fragment used to generate the hash next to the password
const salt = 12;
//  Secret key used to generate our encrypted password
const SECRET_KEY = 'secretkey123456';
const expiresIn = 24 * 60 * 60; // Seconds

const path = require('path');
// Used to delete files
const { unlink} = require('fs-extra');




// Gets all of our restaurants
// Rev put method restauranteInf2 inside of this one calling this method on component home and then passsing the result query to every child component
restauranteCtrl.getRestaurantes = async (req, res, next) => {
    const restaurantes = await Restaurante.find({'bloqueado': false}).sort('nombre');
    res.json(restaurantes);
};

// Create a new restaurant, view-admin, restaurant-profile
restauranteCtrl.createRestaurante = async (req, res, next) => {
    const restaurante = new Restaurante({
        nombre: req.body.nombre,
        logo: req.body.logo,
        slogan: req.body.slogan,
        fotoLocal: req.body.fotoLocal,
        direccion: req.body.direccion,
        fijo: req.body.fijo,
        celular: req.body.celular,
        lat: req.body.lat,
        lng: -req.body.lng,
        fotoMenu: req.body.fotoMenu,
        precioMinimo: req.body.precioMinimo,
        horario: req.body.horario,
        historia: req.body.historia,
        categoria: req.body.categoria,
        servicio: req.body.servicio,
        metodoPago: req.body.metodoPago,
        plato: [],
        bloqueado: true,
        email: req.body.email,
        telefonoContacto: req.body.telefonoContacto,
        contrasena: bcrypt.hashSync(req.body.contrasenaRest, salt)
    });
    await restaurante.save();
    res.json({ status: 200, message: 'Restaurante actualizado' });
};
// Gets an specific restaurant through an id
restauranteCtrl.getRestaurante = async (req, res, next) => {
    const { id } = req.params;
    const restaurante = await Restaurante.findById(id);

    // get arrays of promos by day
    // const promos = await Restaurante.aggregate([
    //     {
    //       '$match': {
    //         '_id': '5c13ed0af911727d03c61d2c'
    //       }
    //     }, {
    //       '$unwind': {
    //         'path': '$plato'
    //       }
    //     }, {
    //       '$unwind': {
    //         'path': '$plato.diaPromocion'
    //       }
    //     }, {
    //       '$group': {
    //         '_id': '$plato.diaPromocion', 
    //         'platos': {
    //           '$push': '$plato'
    //         }
    //       }
    //     }
    //   ]);
    //   console.log(promos)
    res.json(restaurante);
};

// Update a restaurante
restauranteCtrl.editRestaurante = async (req, res, next) => {
    // Checks if accessToken exists
    if(req.body.accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(req.body.accessToken, SECRET_KEY, async (err, decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                const { id } = req.params;
                const currentRestaurant = await Restaurante.findOne({'_id': id}, {contrasena: 1, _id: 0});

                if(currentRestaurant){
                    // If method doesn´t update password
                    var restaurante = {
                        nombre: req.body.nombre,
                        direccion: req.body.direccion,
                        fijo: req.body.fijo,
                        celular: req.body.celular,
                        fotoMenu: req.body.fotoMenu,
                        precioMinimo: req.body.precioMinimo,
                        horario: req.body.horario,
                        historia: req.body.historia,
                        servicio: req.body.servicio,
                        metodoPago: req.body.metodoPago,
                        nombreContacto: req.body.nombreContacto,
                        email: req.body.email,
                        telefonoContacto: req.body.telefonoContacto,
                    };
                    await Restaurante.findByIdAndUpdate(id, { $set: restaurante });
                    res.json({status: 200, message: "Restaurante Actualizado."});
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};

// Delete a restaurant
restauranteCtrl.deleteRestaurante = async (req, res, next) => {
    await Restaurante.findByIdAndRemove(req.params.id);
    res.json({ status: 'Restaurante Deleted' });
};





// Minimap titles: methos for view restaurant
// Rev 2 methods in one
// the first if returns a filtered array of platos by categoria
// the second if returns a filtered array of platos by diaPromocion
// restauranteCtrl.restauranteInf2 = async (req, res, next) => {
//     const { id } = req.params
//     const { param1 } = req.params
//     const { param2 } = req.params

//     // if (id == 4) {
//     //     const restaurantes = await Restaurante.find({ 'plato.categoria': param1, '_id': param2 }, 'plato');
//     //     res.json(restaurantes);
//     // }
//     // Rev will bedeleted get promos because the info its already on front so it will be proccesed on the view
//     // else if (id == 5) {
//     //     const promosDay = await Restaurante.find({ 'plato.diaPromocion': param1, '_id': param2 }, 'plato precio');
//     //     console.log(promosDay)
//     //     res.json(promosDay);
//     // }
// };





// Minimap titles: methods for view admin
// Returns a filter array of restaurants by nombre to view-admin
restauranteCtrl.getFilteredRestaurants = async (req, res, next) => {
    if(req.params.nombre != null){
        const restaurants = await Restaurante.find({'nombre': {$regex: ".*"+ req.params.nombre + ".*", $options:"i"}});
        if(restaurants.length > 0){
            res.json(restaurants);
        }else{
            res.json({ status: 'El restaurante ' + req.params.nombre + ' no existe.' });
        }
    }
};

// returns all (blocked and non-blocked) restaurants
restauranteCtrl.getAllRestaurants = async (req, res, next) => {
    const restaurantes = await Restaurante.find().sort('nombre');
    res.json(restaurantes);
};

// Update an sepecific attribute of restaurant
// Rev fix method so we can updte any param by dataUpdated
restauranteCtrl.updateCampo = async (req, res, next) => {
    const { id } = req.params;
    const { dataUpdated } = req.params;
    const { value } =req.body;
    await Restaurante.findOneAndUpdate({ "_id" : id }, { "bloqueado" : value });
    res.json({ status: 'El restaurante con ' + id + ' esta bloqueado: ' + value + '.'});
};

// Update a restaurante from view admin
restauranteCtrl.editRestauranteFromViewAdmin = async (req, res, next) => {
    // Checks if accessToken exists
    if(req.body.accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(req.body.accessToken, SECRET_KEY, async (err, decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                const { id } = req.params;
                const currentRestaurant = await Restaurante.findOne({'_id': id}, {contrasena: 1, _id: 0});

                if(currentRestaurant){
                    // if admin updates or not password
                    const updatedPassword = req.body.contrasenaRest == currentRestaurant.contrasena;
                    let contrasena = "";
                    if(updatedPassword){
                        // If method doesn´t update password
                        contrasena = currentRestaurant.contrasena;
                    }else{
                        // If method update password
                        contrasena = bcrypt.hashSync(req.body.contrasenaRest);
                    }
                    
                    let restaurante = {
                        nombre: req.body.nombre,
                        direccion: req.body.direccion,
                        fijo: req.body.fijo,
                        celular: req.body.celular,
                        lat: req.body.lat,
                        lng: req.body.lng,
                        precioMinimo: req.body.precioMinimo,
                        categoria: req.body.categoria,
                        email: req.body.email,
                        telefonoContacto: req.body.telefonoContacto,
                        contrasena: contrasena
                    };

                    await Restaurante.findByIdAndUpdate(id, { $set: restaurante });
                    res.json({status: 200, message: "Restaurante Actualizado."});
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};







// Minimap titles: methods for Home
// Rev 3 methods in one
// the first if returns an array to maps
// the second if returns an array to historia
// the third if returns an array to platos and promos
// query bloqueqado: false cause if we created a new restaurant then it will return error
restauranteCtrl.restauranteInf = async (req, res, next) => {
    const { id } = req.params;

    if (id == 1) {
        // return location of every restaurant
        const restaurantes = await Restaurante.find({'bloqueado': false}, '_id nombre fotoLocal lat lng');
        res.json(restaurantes);
    }
    else if (id == 2) {
        // returns historias for every restaurant
        const restaurantes = await Restaurante.find({'bloqueado': false}, '_id nombre historia fotoLocal');
        res.json(restaurantes);
    }
    else if (id == 3) {
        // returns all the platos of every restaurant
        const restaurantes = await Restaurante.find({'bloqueado': false}, '_id, nombre plato');
        res.json(restaurantes);
    }
};





// minimap titles: methos for home 
// Gets all the restaurants with califications
restauranteCtrl.getRestaurantesWithCalifications = async (req, res, next) => {
    const restaurantes = await Restaurante.find({'bloqueado': false}).sort('nombre');
    res.json(restaurantes);
};
// filters
// Returns all the diferent categorias
restauranteCtrl.getDifferentCategorias = async (req, res, next) => {
    const restaurantes = await Restaurante.find({'bloqueado': false}).distinct('categoria');
    restaurantes.sort();
    res.json(restaurantes);
};

// Returns a filtered search of restaurants for home componentes (historias, platos, promos y restaurantes)
restauranteCtrl.getFilteredSearch = async (req, res, next) => {
    let { selectedCategoria } = req.params;
    let { selectedPrecio } = req.params;    
    let { restaurantsAboveCalification } = req.params;

    // data setting
    let filter = {};
    let restaurantes;
    if(selectedCategoria != "0"){
        filter["categoria"] = selectedCategoria;
    }
    if(selectedPrecio != "0" && selectedPrecio != "4"){
        // Rev when filtering by selected precio I need to filter by plato, not by restaurant
        filter["precioMinimo"] = {$lte: selectedPrecio};
    }
    if(restaurantsAboveCalification != [] && restaurantsAboveCalification != "0"){
        // convert restaurantsAboveCalification to json to work
        restaurantsAboveCalification = JSON.parse(restaurantsAboveCalification);
        filter["_id"] = {$in: restaurantsAboveCalification};
    }

    filter["bloqueado"] = false;

    restaurantes = await Restaurante.find(filter);
    res.json(restaurantes);
};





// Methods for login
// Method used to login a restaurant by a secure way
restauranteCtrl.loginRestaurante = async (req, res, next) => {
    const restaurantData = {
      dataLogin: req.body.email,
      password: req.body.contrasena
    }

    await Restaurante.findOne({ email: restaurantData.dataLogin }, async (err, restaurante) => {
      if (err) return res.json({ status: 500, message: "Server error."});
  
      if (!restaurante) {
        // Email does not exist
        // By security we didn´t tell that the email dosen´t exists
        res.json({ status: 409,  message: 'E-mail incorrecto.' });
      } else {
        const resultPassword = bcrypt.compareSync(restaurantData.password, restaurante.contrasena);
        if (resultPassword) {
            // The ideal way is to not use our database _id, its better if we used another id
            // de esta forma no ponemos en riesgo la integridad de nuestra BD
            let date = new Date();
            date = date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds();
            const accessTokenName = jwt.sign({ tokenData: restaurante.nombre + date},
                SECRET_KEY, {
                    expiresIn: expiresIn
                }); 

            await Restaurante.findOneAndUpdate({ _id: restaurante._id}, { accessToken: accessTokenName });
            // response
            res.json( { status: 200,  restaurante: restaurante, accessToken: accessTokenName });
        } else {
          // password wrong
          // By security we didn´t tell that passwords dosen´t match
          res.json({ status: 409,  message: 'Contraseña incorrecta.' });
        }
      }
    });
};

//Method used to logout a restaurant by a secure way and delete accestoken
restauranteCtrl.logoutRestaurante = async (req, res, next) => {
    // Checks if accessToken exists
    if(req.body.accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(req.body.accessToken, SECRET_KEY, async (err,decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                let usuario = await Restaurante.findOneAndUpdate({ accessToken: req.body.accessToken}, { accessToken: null });
                if(usuario){
                    res.json({status: 200, message: "Restaurante con accessToken " + req.body.accessToken + " ha sido deslogueado."});
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};

// Returns a specific restaurant by accessToken
restauranteCtrl.getRestaurantByToken = async (req, res, next) => {
    const { accessToken } = req.params;


    // Checks if accessToken exists
    if(accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(accessToken, SECRET_KEY, async (err,decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                const restaurant = await Restaurante.findOne({'accessToken': accessToken});
                if(restaurant){
                    res.json({status: 200, restaurant: restaurant});
                }else{
                    res.json({status: 400, message: 'Ha ocurrido un error con su sesión.'});
                }
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};

// Changes password
restauranteCtrl.changePassword = async (req, res, next) => {
    const { accessToken } = req.body.userInfo;

    // Checks if accessToken exists
    if(accessToken != 'null'){
        // Checks if accessToken its valid
        jwt.verify(accessToken, SECRET_KEY, async (err,decoded) => {
            if(!err){
                // If accessToken is valid, then work as usual
                await Restaurante.findById( req.params.id, async (err, restaurante) => {
                    if (!restaurante) {
                        // User doesn´t exists
                        // By security we didn´t tell that the username or email dosen´t exists
                        res.json({ status:409, message: 'Algo anda mal, vuelve a intentarlo más tarde.'});
                    }else{
                        // User exists
                        const resultPassword = bcrypt.compareSync(req.body.userInfo.currentPassword, restaurante.contrasena);            
                        if(resultPassword){
                            await Restaurante.findOneAndUpdate({ _id: req.params.id }, { contrasena: bcrypt.hashSync(req.body.userInfo.newPassword, salt) });
                            res.json({ status:200, message: "La contraseña del restaurante " + restaurante.nombre + " ha sido cambiada exitosamente." });
                        }else{
                            res.json({ status: 409, message: 'Algo anda mal, vuelve a intentarlo más tarde.'});
                        }
                        
                    }
                });
            }else{
                // If accessToken isn´t valid, unahutorized the user
                res.json({status: 401, message: 'Desautorizado'});
            }
        });
    }else{
        res.json({status: 400, message: 'No ha enviado un accessToken'});
    }
};




// Minimap titles: Methods for view profile restaurant

// Used for aws upload images on buckets s3
var AWS = require('aws-sdk')
var s3 = new AWS.S3({
    accessKeyId: global.gConfig.accessKeyId,
    secretAccessKey: global.gConfig.secretAccessKey
});
// name image wich will be send to DB
var image = "";
// Upload files to aws s3 bucket
async function uploadFileAws(route, file){
    // Params used to deploy images on bucket s3 aws
    const paramsUpload = {
        Bucket: global.gConfig.bucket + route,
        Key: Date.now().toString() + path.extname(file.originalname), // File name you want to save as in S3
        Body: file.buffer,
        ACL: 'public-read',
        ContentType: 'image'
    };

    // Rev fix file assignation, if file cannot be upload then we will have a problem
    // its neccesary to wait until file its upload to get a real url a not a "url quemada"
    // best way its to put update query inside s3.upload, not done because s3.upplaod tackes so much time and the update looks slow
    image = paramsUpload.Key;

    // Uploading files to the bucket
    await s3.upload(paramsUpload, async (err, data) => {
        if (err) {
            console.log(err);
        }
        console.log(`File uploaded successfully. ${data.Location}`);
        
        // Rev file assign should work like this, but upload the file take so much time
        // file = data.Location;
    });
}

// delete files from aws s3 bucket
let deleteFile = async function deleteFileAws(route, oldFile){
    var paramsDelete = {  
        Bucket: global.gConfig.bucket + route,
        Key: oldFile
    };

    s3.deleteObject(paramsDelete, function(err, data) {
        if (err) console.log(err, err.stack);
        else     console.log('Deleted succesfully');
    });
}

// creates local image
restauranteCtrl.changePhotoLocal = async (req, res) => {
    const { id } = req.params;
    // used for multer OLD METHOD
    // const file = '/public/uploads/restaurants/local/' + req.file.filename;

    let route = '/restaurants/locals';

    uploadFileAws(route, req.file);

    let restaurante = await Restaurante.findOneAndUpdate({ "_id" : id }, { fotoLocal: image });    

    deleteFile(route, restaurante.fotoLocal);

    // used for multer OLD METHOD
    // if(restaurante.fotoLocal){
    //     await unlink(path.resolve('./Back' + restaurante.fotoLocal));
    // }
    res.json({ status: 200, message: 'Foto local creado.' });
}

// Creates logo image
restauranteCtrl.changePhotoLogo = async (req, res) => {
    const { id } = req.params;
    // used for multer OLD METHOD
    // const file = req.file.path;
    
    let route = '/restaurants/logos';

    uploadFileAws(route, req.file);

    let restaurante = await Restaurante.findOneAndUpdate({ "_id" : id }, { logo: image });

    deleteFile(route, restaurante.logo);

    // used for multer OLD METHOD
    // if(restaurante.logo){
    //     await unlink(path.resolve(restaurante.logo));
    // }
    res.json({ status: 200, message: 'Logo creado.' });
}

// Create a plate for an specific restaurant
restauranteCtrl.createPlate = async (req, res, next) => {
    const { id } = req.params;
    const restaurantCategories = JSON.parse(req.body.restaurantCategories);
    // const file = req.file.path;

    let route = '/restaurants/plates';

    uploadFileAws(route, req.file);

    const plato = {
        nombre: req.body.nombre,
        categoria: JSON.parse(req.body.categoria),
        descripcion: req.body.descripcion,
        precio: req.body.precio,
        diaPromocion: JSON.parse(req.body.diaPromocion),
        foto: image
    }
    // Rev problem with plato null instead of array typeof, should I send plato as [] when creating a restaurant?
    await Restaurante.findOneAndUpdate({ "_id" : id },
        {$set:
            {
                categoria: restaurantCategories
            },
            $push:
                { plato: plato}
        });

    res.json({ status: 200, message: 'Plato creado.' });
};

// Create a plate for an specific restaurant
restauranteCtrl.deletePlate = async (req, res, next) => {
    const { id } = req.params;
    const { photoPlate } = req.body;
    let route = '/restaurants/plates';

    // let restaurante = await Restaurante.findOneAndUpdate({ "_id" : id, "plato.foto": photoPlate }, { $unset: { "plato.$": "" } });
    let restaurante = await Restaurante.findOneAndUpdate({ "_id" : id  }, { $pull: { plato: { foto: photoPlate } } });
    
    if(restaurante){
        // await unlink(path.resolve(photoPlate));
        deleteFile(route, photoPlate);
        res.json({ status: 200, message: 'Plato eliminado.' });
    }else{
        res.json({ status: 400, message: 'Ooops. Algo ocurrio.' });
    }
};



module.exports = restauranteCtrl;