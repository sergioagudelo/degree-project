const mongoose = require('mongoose');
const { Schema } = mongoose;

// Rev usuario should by an id and not the username

// Rev fix model ResenaSchema like this ->
// const ResenaSchema = new Schema({
//     restaurante: String,
//     resena : Array
// }, {
//     timestamps: true
// });
const ResenaSchema = new Schema({
    restaurante: String,
    usuario: String,
    texto: {
        type: String,
        required: true
    },
    calificacion: Number,
    fecha: { type: Date, default: Date.now }
}, {
    timestamps: true
});

module.exports = mongoose.model('Resena', ResenaSchema);