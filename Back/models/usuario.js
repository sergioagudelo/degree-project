const mongoose = require('mongoose');
const { Schema } = mongoose;

// Solve (node:2596) DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.
mongoose.set('useCreateIndex', true);

const UsuarioSchema = new Schema({
    fotoPerfil: {
        type: String,
        trim: true,
    },
    usuario: {
        type: String,
        trim: true,
        unique: true
    },
    nombre: {
        type: String,
        trim: true,
        required: true
    },
    correo: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    fechaNacimiento: {
        type: String,
        trim: true,
    },
    celular: {
        type: String,
        trim: true
    },
    contrasena: {
        type: String,
        trim: true
    },
    bloqueado: {
        type: Boolean,
        trim: true,
    },
    accessToken: {
        type: String,
        trim: true
    },
    provider: {
        type: String,
        trim: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Usuario', UsuarioSchema);