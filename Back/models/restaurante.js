// Data model for restaurants
const mongoose = require('mongoose');
// We used fron module mongoose only its Schemas
// We used Schemas to define the restaurants data model
const { Schema } = mongoose;

// RestauranteSchema is the way our model looks, isnt the real data
const RestauranteSchema = new Schema({
    nombre: {
        type: String,
        trim: true
    },
    logo: {
        type: String,
        trim: true
    },
    slogan: {
        type: String,
        trim: true
    },
    fotoLocal: {
        type: String,
        trim: true
    },
    direccion: {
        type: String,
        trim: true
    },
    fijo: {
        type: String,
        trim: true
    },
    celular: {
        type: String,
        trim: true
    },
    lat: {
        type: Number,
        trim: true
    },
    lng: {
        type: Number,
        trim: true
    },
    fotoMenu: {
        type: String,
        trim: true
    },
    precioMinimo: {
        type: Number,
        trim: true
    },
    horario: {
        type: Array,
        trim: true
    },
    historia: {
        type: String,
        trim: true
    },
    categoria: {
        type: Array,
        trim: true
    },
    servicio: {
        type: Array,
        trim: true
    },
    metodoPago: {
        type: Array,
        trim: true
    },
    plato: {
        type: Array,
        trim: true
    },
    bloqueado: {
        type: Boolean,
        trim: true
    },
    // datos de contacto propietario o representante
    nombreContacto: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    telefonoContacto: {
        type: String,
        trim: true
    },
    // End datos de contacto propietario o representante
    contrasena: {
        type: String,
        trim: true,
        required: true
    },
    accessToken: {
        type: String,
        trim: true
    }
}, {
    timestamps: true
});

// We passed RestauranteSchema as a mongoose model
module.exports = mongoose.model('Restaurante', RestauranteSchema);