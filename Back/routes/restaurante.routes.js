// Routes used by the server to work with restaurants
const { Router } = require('express');
// Object from express used to manipulate routes
const router = Router();
// Rev fix name restaurante to restauranteCtrl, isnt and object
const restaurante = require('../controllers/restaurante.controller');




// Imports object multer used to work with images
const uploadsPlate = require('../libs/multer/restaurantes/multerPlates');
const uploadsLocal = require('../libs/multer/restaurantes/multerLocal');
const uploadsLogo = require('../libs/multer/restaurantes/multerLogo');





// Routes used to access to our restaurants.
// Gets all of our restaurante
router.get('/', restaurante.getRestaurantes);
// Create a new restaurant
router.post('/', restaurante.createRestaurante);
// Gets an specific restaurant through an id
// rev router.get('/:id', restaurante.getRestaurante);
router.get('/getRestaurante/:id', restaurante.getRestaurante);
// Update a restaurante, view-admin and restaurant-profile
router.put('/editRestaurante/:id', restaurante.editRestaurante);
// Delete a restaurant
router.delete('/:id', restaurante.deleteRestaurante);
// Returns restaurantrs for: maps, historias or platos
router.get('/restauranteInf/:id', restaurante.restauranteInf);
// // Returns platos by categoria or platos by diaPromocion
// router.get('/:id/:param1/:param2', restaurante.restauranteInf2);





// Minimaps titles: Routes for view-admin
// Returns restaurants filtered by nombre
router.get('/getFilteredRestaurants/:nombre', restaurante.getFilteredRestaurants);
// Update an sepecific attribute of Restaurant
router.put('/updateCampo/:id/dataUpdated/:dataUpdated', restaurante.updateCampo);
// Returns all (blocked and non-blocked) restaurants
router.get('/getAllRestaurants', restaurante.getAllRestaurants);
// Update a restaurante, view-admin and restaurant-profile
router.put('/editRestauranteFromViewAdmin/:id', restaurante.editRestauranteFromViewAdmin);




// Minimap titles: Methods for view profile restaurant
// creates local image
// fix to send both images (local and logo) on one multer with multer.array
router.put('/changePhotoLocal/:id', uploadsLocal.single('local'), restaurante.changePhotoLocal);
// Create and logo images
router.put('/changePhotoLogo/:id',  uploadsLogo.single('logo'), restaurante.changePhotoLogo);
// Create a plate for an specific restaurant
router.put('/createPlate/:id', uploadsPlate.single('image'), restaurante.createPlate);
// Deletes an specific plate for an specific rerstaurant
router.delete('/deletePlate/:id', restaurante.deletePlate);





// Minimaps titles: Routers for Home filters
// Gets all the restaurants with califications
router.get('/getRestaurantesWithCalifications', restaurante.getRestaurantesWithCalifications);
// filters
// Returns all the diferent categorias
router.get('/getDifferentCategorias', restaurante.getDifferentCategorias);
// Retunrs filtered search for Home
router.get('/getFilteredSearch/selectedCategoria/:selectedCategoria/selectedPrecio/:selectedPrecio/restaurantsAboveCalification/:restaurantsAboveCalification'
            , restaurante.getFilteredSearch);





// Minimap-titles: METHOS USED ON LOGIN
// Login an rextaurant with jwt by email
router.post('/loginRestaurante', restaurante.loginRestaurante);
// Logout rextaurant and delete accesstoken on DB
router.put('/logoutRestaurante', restaurante.logoutRestaurante);
// Returns a specific restaurant by nombre, old method
router.get('/getRestaurantByToken/:accessToken', restaurante.getRestaurantByToken);
// ChangePassword of a restaurant
router.put('/changePassword/:id/currentPassword/', restaurante.changePassword);





module.exports = router;