const express = require('express');
const router = express.Router();
const resena = require('../controllers/resena.controller');

router.get('/', resena.getResenas);
router.post('/', resena.createResena);
router.get('/getResenas/:id', resena.getResena);
router.put('/:id', resena.editResena);
router.delete('/delete/:id', resena.deleteResena);
  // Returns the califications average for a restaurant
router.get('/getCalificacionRestaurante/:id', resena.getCalificacionRestaurante);





// minimaps titles: methos used to filter Home
// Returns an array of reestaurants based on califictions
router.get('/getRestaurantsAboveCalification/:selectedCalificacion', resena.getRestaurantsAboveCalification);





// minimap titles: view admin
// Returns a filtered array of resenas
router.get('/getFilteredResenas/:usuario', resena.getFilteredResenas);


module.exports = router;