const { Router } = require('express');
const router = Router();
const email = require('../controllers/email.controller');

// email sended from contacto component
router.post('/contacto', email.sendEmailContacto);
// email sended from recoverPassword component
router.post('/sendEmailRecoverPassword', email.sendEmailRecoverPassword);

module.exports = router;