const { Router } = require('express');
const router = Router();
const usuario = require('../controllers/usuario.controller');




// Imports object multer used to work with images
const uploads = require('../libs/multer/usuarios/multer');



// Routes
router.get('/', usuario.getUsuarios);
router.post('/', usuario.createUsuario);
router.put('/:id', uploads.single('image'), usuario.editUsuario);
router.delete('/:id', usuario.deleteUsuario);
// Returns a filtered array used to show an specific user
router.get('/getFilteredUsers/:dataSearch/inputSelected/:inputSelected', usuario.getFilteredUsers);
// Update an sepecific attribute of user
// Rev its better if we make diferent methods per view (update msWriteProfilerMark, adminusers)
router.put('/updateCampo/:id/dataUpdated/:dataUpdated', usuario.updateCampo);
// Returns a specific user by username or email, old method, new method loginUser
router.get('/getUser/:id', usuario.getUser);





// Minimap-titles: METHOS USED ON LOGIN
// Register an user with jwt
router.post('/register', uploads.single('image'), usuario.registerUser);
// Login an user with jwt by username (1) or email (2)
router.post('/login', usuario.loginUser);
// Logout user and delete accesstoken on DB
router.put('/logout/:accessToken', usuario.logoutUser);
// Returns a specific user by username or email
router.get('/getUserByAccessToken/:accessToken', usuario.getUserByAccessToken);
// Returns a specific user by email and update his password
router.get('/getUserByEmailAndUpdatePassword/:email/newPassword/:newPassword', usuario.getUserByEmailAndUpdatePassword);
// changePassword of an user
router.put('/changePassword/:id/currentPassword/:currentPassword', usuario.changePassword);
// Login or Register an user by redes
router.post('/loginWithRedes/:userName', usuario.loginWithRedes);


module.exports = router;