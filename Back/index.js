// Imports config.js so the script its executed
const config = require('./config/config.js');
const mongoose = require('mongoose');
const express = require('express');
// Cors is a package used to connect my server front with my server back
const cors = require('cors');
const path = require('path');
const app = express();
const morgan = require('morgan');




// This folder for this application will be used to store public files
// Rev delete this command when finished fizing new way to work with images
// worked with multer, but now working with aws s3 buckets
// app.use('/Uploads', express.static(path.resolve('Uploads')));



// Settings - server configuration.
// Rev I think this line is innecesary
process.env.NODE_ENV = 'production';

// Returns as a json the config params on the route get.config/params, checks local o production aprams
app.get('/config/params/', (req, res) => {
    res.json(global.gConfig);
});

// Connects to global.gConfig.database setted on config.json
mongoose.connect(global.gConfig.database, { useNewUrlParser: true })
    .then(db => console.log('db is connected ' + global.gConfig.database))
    .catch(err => console.error(err));

// This variable its used to set a variable 'port' that its the port on which we are working our backend
// process.env.PORT - takes the port given by the cloud service.
app.set('port', process.env.PORT || global.gConfig.node_port);



// Middlewares
// This sentence allows comunication bewteen config.json.front.corsUrl and back end port 3000 or given by server
app.use(cors({ origin: [global.gConfig.corsUrl, 'http://localhost:4200'] }));
// app.use(cors({ origin: 'http://localhost:4200'}));
// Translate code from web  to server (Json)
app.use(express.json());
app.use(morgan('dev'));





// Routes - server routes.
// Route used to send emails
app.use('/api/contacto', require('./routes/email.routes'));
// Route used to work with restaurants.
app.use('/api/restaurantes', require('./routes/restaurante.routes'));
// Route used to work with users
app.use('/api/usuarios', require('./routes/usuario.routes'));
// Route used to work with reviews
app.use('/api/resenas', require('./routes/resena.routes'));

// Route used to work in generated folfer dist for production
// Dirname its used to get the route to the actual file to work as static files
if(global.gConfig.config_id === 'production'){
    app.use(express.static(path.join(__dirname, 'dist/Front')));
}

app.get('*', (request, response) => {
	response.sendFile(path.join(__dirname, 'dist/Front', 'index.html'));
});

// // Static files (images).
// worked with multer, but now working with aws s3 buckets
// app.use(express.static(path.join(__dirname, 'public')));

// Starts the server
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});

// Rev el packaje.json del back, guarda toda al cofiguracion del proyecto, nos puede facilitar la vida implementar bien este archivo (carlos estevez).
// Rev used packages:
// express, nodemon, morgan.

//Rev https://www.youtube.com/watch?v=KyyzhRqkJRA&t=275s how to configure better our index.js