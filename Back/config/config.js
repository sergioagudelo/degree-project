const _ = require('lodash');
const config = require('./config.json');
const defaultConfig = config.development;
// Sets enviroment as production (if the there is a server giving us a enviroment) or development
// NODE_ENV: determine whether enviroment is running in a "development" environment vs a "production" environment
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);

// Sets global (global for the whole back-end nodejs) variable
global.gConfig = finalConfig;